  <!DOCTYPE html>
  <html lang="en">
  <head>
  <title>Contact</title>

   <link href="assets/css/bootstrap.css" rel="stylesheet">
   <link href="css/styles.css" rel="stylesheet">
   <link rel="stylesheet"  href="css/bootstrap.css">
   <link rel="stylesheet" href="css/contact.css">
   <link rel="stylesheet" href="css/styles.css">
   <link rel="stylesheet" href="css/styles-squad.css">
   <link rel="stylesheet" href="css/ionicons.min.css">
   <link rel="stylesheet" href="css/jquery-ui.css">
   <link rel="stylesheet" href="css/font-awesome.min.css">
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
   <link rel="icon" type="image/png" href="favicons/favicon-32x32.png" sizes="32x32">
   <link rel="icon" type="image/png" href="favicons/favicon-16x16.png" sizes="16x16">

 </head>
 <body>

  <!-- ******************** NAV *********************** -->
  <?php 
  include('connect_to_base.php');
  include('nav_lin.php');?>

<!-- ******************** SQUAD *********************** -->
<?php 
 include_once("connect_to_base.php") ;
if( (!empty($_POST)) && (isset($_POST))  ) {
  $email = "contact@asm.tn"; // A changer
  $header= "FROM : ".$_POST['name']."<".$_POST['email'].">";
  $subject = $_POST['subject'];
  $body= $_POST['message']."\n Téléphone : ".$_POST['tel'];
  mail($email, $subject, $body, $header) ;
}
 ?>
<div class="container main-slider" id="asm">
  <div class="col-lg-5 cform-container">
    <div class="formu">
    <h2>AVENIR SPORTIF DE LA MARSA</h2>
    <p>lorem lipsum forum lopreup lipsum intoum forum diupxum xxforum</p>
    <hr>
    <form action="contact.php" method="post">
    <table >
      <tr>
      <h4>Nom :</h4>
          <div class="input-group">
  <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-user" aria-hidden="true"></span></span>
  <input type="text" name="name" class="form-control widthizer" placeholder="Nom" aria-describedby="basic-addon1">
</div>
      </tr>
      <tr>
      <h4 class="longer">Téléphone :</h4>
          <div class="input-group">
  <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span></span>
  <input type="number" name="tel" class="form-control widthizer" placeholder="Téléphone" aria-describedby="basic-addon1">
</div>
      </tr>
      <tr>
      <h4>Email :</h4>
           <div class="input-group">
  <span class="input-group-addon" id="basic-addon1">@</span>
  <input type="email" name="email" class="form-control widthizer" placeholder="Email" aria-describedby="basic-addon1">
</div>
      </tr>
      <tr>
      <h4 class="longer">Message :</h4>
          <div class="input-group">
  <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></span>
  <textarea name="message" id="message" cols="30" rows="2" class="form-control widthizer" aria-describedby="basic-addon1" placeholder="Je vous contacte pour une offre ... "></textarea>
</div>
      </tr>
    
    </table>
    <button class="btn submitter" type="submit">Envoyer votre message</button>
    </form>
  </div>

  </div>

   
       <div class="col-lg-5 col-lg-offset-2">
    <p class="right_side_text">
     Lorem lipsum <br> 
     22, lipsum 2025 lipsum forum onthum  <br>
     La marsa  Tunis, Tunisia
    </p>
   </div>
  
</div>
<div id="default" class="map"></div>
<!-- ******************** FOOTER *********************** -->

<?php include('footer.php');?>
<!-- ******************** JS *********************** -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/custom-squad.js"></script>
<script src="js/draggable_background.js"></script>
<script>
    $(function() {
      $('#default').backgroundDraggable();
    });
  </script>


</body>
</html>