$(document).ready(function(){

  $('.suivi').slick({

  	slide: 'ul',
  	prevArrow: $('.prev-btn'),
  	nextArrow: $('.next-btn'),
  	asNavFor: '.slider-part2',
  	infinite: false,
    draggable: false

    });
  $('.slider-part2').slick({
  	arrows: false,
  	infinite: false,
    draggable: false
  });
  $('.histoire').slick({
    prevArrow: $('.prev-button'),
    nextArrow: $('.next-button'),
    infinite: false,
    draggable: false
  });

  var prizeNumber = $('.prize').length ;
  if(prizeNumber > 3) {
    $('#prize-slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesPerRow: 1,
      centerMode: true,
      autoplay: true,
      autoplaySpeed: 2000,
      speed: 500,
      arrows: false,
      cssEase: 'ease-out',
      mobileFirst: true,
      responsive: [
      {
       breakpoint: 768,
       settings: {
         arrows: false,
         centerMode: true,
         centerPadding: '40px',
         slidesToShow: 3,
         slidesPerRow: 3,
         variableWidth: false
       }
     },
     {
       breakpoint: 480,
       settings: {
         arrows: false,
         centerMode: true,
         centerPadding: '80px',
         slidesToShow: 1,
         slidesPerRow: 1
       }
     }
     ]
   });
  }
});
