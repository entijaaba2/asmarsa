<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<style>
td{
	padding-top: 15px;
}
</style>
</head> 
   
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$page="history";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php");
			
	$actu="";
	if(!empty($_GET)){

				if($hist=$bdd->query('SELECT * FROM history as his, category as cate WHERE his.hid="'.$_GET['id'].'" AND cate.cat=cl.cat AND cate.visibility=1')->fetch());
				else {
					header('Location: allHistory.php');
					die();
				}
				
				} else {
					header('Location: allHistory.php');
					die();
				}
	?>
	<title><?php echo $hist['title'];?> - Admin</title>


			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="switches">
		<div class="col-lg-2 col-lg-offset-10">
		<a href="editHistory.php?id=<?php echo $hist['hid'];?>"><button class="btn btn-success">Modifier</button></a>
		</div>
		<div class=" col-lg-12">
		<h1><?php echo $hist['title'];?></h1>
		
		<ol class="breadcrumb">
							  <li><a href="allHistory.php">Histoire&Légende</a></li>
							  <li><a href="allHistory.php?section=<?php echo $hist['cat'];?>"><?php echo $hist['cat'];?></a></li>
							  
							</ol>
		<div class="col-lg-3">
		<img src="../img/history/<?php echo $hist['hid'];?>.jpg" style="max-width : 300px;">
		</div>
		<div class="col-lg-9">
		<h3><?php echo $hist['bgtitle'];?></h3>
		<div class="col-lg-12" style="text-align: justify; text-justify: inter-word; "><?php echo html_entity_decode($hist['text']);?></div>
		</div>
      
	
		</div>
		
		  

		
		
		</div>
		</div>
		
		
		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
   
 <!-- JS Files -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

</script>


<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>

</body>
</html>