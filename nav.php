  <nav class="navbar navbar-default">
    <div class="row">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a class="navbar-brand" href="#">
          <img alt="Brand" src="images/logo_asm.svg" width="23%">
        </a>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar top-bar"></span>
          <span class="icon-bar middle-bar"></span>
          <span class="icon-bar bottom-bar"></span>
        </button>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

        <ul class="nav navbar-nav main-list">
          <li><a href="index.php"><i class="ionicons ion-heart"> <p>ASM</p></i></a></li>
          <li><a href="collectif.php?section=football"><i class="ionicons ion-ios-football"> <p>FOOTBALL</p></i></a></li>
          <li>
          <a href="#sportscol" id="menu">
          <i class="ionicons ion-ios-basketball">
          <p>SPORTS COLLECTIFS</p>
          </i>
          </a>
          </li>
          <li><a href="#sportsind" id="menu2"><i class="ionicons ion-ios-tennisball" > <p>SPORTS INDIVIDUELS</p></i></a></li>
          <li><a href="contact.php"><i class="ionicons ion-information-circled"> <p>INFO</p></i></a></li>
        </ul>
        
        <div class="dropdown2" id="menu-box2">
          <div class="dropdown-content2 text-center">

            <div class="col-md-8">
              <a href="#">Tennis</a>
              <a href="#">Natation</a>
              <a href="#">Petanque</a>
              <a href="#">Athlétisme</a>
              <a href="#">Lutte</a>
            </div>
          </div>
          <div class="dropdown-footer">
            <ul>
              <li><a href=""><i class="fa fa-facebook"></i></a></li>
              <li><a href=""><i class="fa fa-twitter"></i></a></li>
              <li><a href=""><i class="fa fa-youtube-square"></i></a></li>
              <li><a href=""><i class="fa fa-instagram"></i></a></li>
            </ul>
            <ul class="pull-left">
             <?php 
        include("connect_to_base.php");
        foreach (glob("img/SponsorLogos/alogo*") as $filename) {
    ?>
               <li><img src="<?php echo $filename;?>" width="14"></li>
              <?php  } ?>
            </ul>
          </div>
          </div>
          <div class="dropdown" id="menu-box">
          <div class="dropdown-content text-center">
            <div class="col-md-8">
            <?php $categories=$bdd->query('SELECT cat FROM category WHERE visibility=1')->fetchAll();
            $nb=count($categories);
            $i=0;
            foreach ($categories as $key) {
              $i++;
              echo '<a href="collectif.php?section='.$key['cat'].'">'.$key['cat'].'</a>';
              if($i % 2 ==0) {
                if($i==$nb) echo '</div>'; else echo '</div><div class="col-md-4">';
              } 
              
            }?>
              
          </div>
          <div class="dropdown-footer">
            <ul>
              <li><a href=""><i class="fa fa-facebook"></i></a></li>
              <li><a href=""><i class="fa fa-twitter"></i></a></li>
              <li><a href=""><i class="fa fa-youtube-square"></i></a></li>
              <li><a href=""><i class="fa fa-instagram"></i></a></li>
            </ul>
            <ul class="pull-left">
            <?php 
        foreach (glob("img/SponsorLogos/alogo*") as $filename) {
    ?>
               <li><img src="<?php echo $filename;?>" width="14"></li>
              <?php  } ?>
            </ul>
          </div>
          </div>


        <ul class="sponsors-nav nav navbar-nav navbar-left">
        <?php 
        foreach (glob("img/SponsorLogos/alogo*") as $filename) {
    ?>
   
          <li><img src="<?php echo $filename;?>" width="25"></li>
    <?php  } ?>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li>
            <form name="nav-search-form" method="get" action="recherche.php">
              <input type="text" placeholder="Recherche" name="query" id="search-input">
            </form>
          </li>
          <li><span id="search-glyph" class="glyphicon glyphicon-search"></span></li>
          <li><img src="images/language.png" alt="Langue"></li>
          <li><p>mon compte</p></li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
    <div class="container-fluid">
      <div class="row">
        <div class="circle-container">
          <ul>
            <a href="#"><div class="logo-nav center" title="ASM Logo"></div></a>
            <a href='#' class='deg0'><li><i class="ionicons ion-information-circled"> <p>INFO</p></i></li></a>
            <a href='#' class='deg45'><li><i class="ionicons ion-ios-tennisball"> <p>SPORTS INDIVIDUELLES</p></i></li></a>
            <a href='#' class='deg90'><li><i class="ionicons ion-ios-basketball"> <p>SPORTS COLLECTIFS</p></i></li></a>
            <a href='collectif.php?section=football' class='deg135'><li><i class="ionicons ion-ios-football"> <p>FOOTBALL</p></i></li></a>
            <a href='index.php' class='deg180'><li><i class="ionicons ion-heart"> <p>ASM</p></i></li></a>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <img class="nav-shape img-responsive" src="images/nav_shape.png">