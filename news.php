  <!DOCTYPE html>
  <html lang="en">
  <head>
  <title>Contact</title>

   <link href="assets/css/bootstrap.css" rel="stylesheet">
   <link rel="stylesheet"  href="css/bootstrap.css">
   <link rel="stylesheet" href="css/contact.css">
   <link rel="stylesheet" href="css/styles.css">
   <link rel="stylesheet" href="css/ionicons.min.css">
   <link rel="stylesheet" href="css/jquery-ui.css">
   <link rel="stylesheet" href="css/font-awesome.min.css">
   <link rel="stylesheet" href="css/news.css">
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
   <link rel="icon" type="image/png" href="favicons/favicon-32x32.png" sizes="32x32">
   <link rel="icon" type="image/png" href="favicons/favicon-16x16.png" sizes="16x16">

 </head>
 <body>

  <!-- ******************** NAV *********************** -->
  <?php 
  
  include_once('connect_to_base.php');
  $req=$bdd->query('SELECT * FROM actualite WHERE acid='.$_GET['id']);
  $sth = $bdd->prepare('SELECT * FROM actualite WHERE acid = ?');
  $sth->execute(array($_GET['id']));
  $data = $sth->fetch();
  if((empty($data)) || empty($_GET['id']) || (!isset($_GET['id']))) {header('Location: index.php');}
  include('nav_lin.php');
  ?>

<!-- ******************** NEWS *********************** -->

<div class="container news-container" style="background-image: url('<?php echo "img/actualite/".$data[0].".jpg";?>');">
  <h1 class="news-title"><?php echo (($data['title'])); ?></h1>
  <div class="news-description">
    <?php echo html_entity_decode($data['article']); ?>
  </div>
</div>

<!-- ******************** FOOTER *********************** -->

<?php include('footer.php');?>
<!-- ******************** JS *********************** -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/custom-squad.js"></script>
<script src="js/news.js"></script>


</body>
</html>