<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Toutes les Histoires&Légendes - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">


</head> 
   
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$page="history";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="col-lg-9">
		<h2>Liste des Histoires & Légendes:</h2>
		</div>
		<div class="col-lg-3">
		<a href="addHistory.php"><button class="btn btn-success">Ajouter une Histoire&Légende</button></a>
		</div>
	
		<div class="switches">
		<div class=" col-lg-12">
		<ul class="nav nav-tabs">
		<?php 
		include_once("connect_to_base.php");
		$req=$bdd->query('SELECT * FROM category  WHERE visibility=1')->fetchAll();
		$activ=false;
		foreach ($req as $data) {
		?>
  <li  <?php if(!$activ) {echo 'class="active"'; $activ=true;}?>><a data-toggle="tab" href="#<?php echo $data['cat'];?>"><?php echo $data['cat'];?></a></li>
		<?php  } ?>
</ul>

<div class="tab-content">
<?php 	$s=false; $i=0;
		foreach ($req as $data0) {
		$i++;	
		?>
  <div id="<?php echo $data0['cat'];?>" class="tab-pane fade <?php if (!$s) {$s=true; echo 'fade in active';}?>">
    <h3><?php echo $data0['cat'];?></h3>
    
	 <table id="example<?php echo $i;?>" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Photo</th>
				<th>Titre</th>
                <th>Grand titre</th>
                <th>Texte</th>
                <th>Gérer</th>
                
            </tr>
        </thead>
        
        <tbody>
		<?php
		$req1=$bdd->query('SELECT * FROM history WHERE cat="'.$data0['cat'].'" ORDER BY hid DESC');
		while($data1=$req1->fetch()){
		?>
         <tr>
                <td><a href="history.php?id=<?php echo $data1['hid'];?>"><img src="../img/history/<?php echo $data1['hid']; ?>.jpg" height="150" width="100"></td>
                <td><a href="history.php?id=<?php echo $data1['hid'];?>"><?php echo $data1['title']; ?></td>
                <td><?php echo $data1['bgtitle']; ?></td></a></td>
                <td><?php echo substr((strip_tags(html_entity_decode($data1['text']))),0,70); ?></td>
                <td  style="text-align : center;">
				<a href="deleteHistory.php?id=<?php echo $data1['hid'];?>" data-toggle="tooltip" title="supprimer l'Histoire&Légende"><img src="./images/trash.png"></a>
				<a href="editHistory.php?id=<?php echo $data1['hid'];?>" data-toggle="tooltip" title="modifier l'Histoire&Légende"><img src="./images/cog.png"></a>
				</td>
                
            </tr>
		<?php } ?>
		</tbody>
		</table>
		
		
	
  </div>
 <?php  } ?>
</div>
		<br>
     
		</div>
		</div>
		
		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
   

 <!-- JS Files -->
<script src="js/jquery.js"></script>
 <script src="js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
	var counter="<?php echo $i;?>";
	var tabnb;
	for(var i=0;i<=counter;i++){
		tabnb='#example'+i;
		$(tabnb).DataTable();
	}
	if(window.location.search.indexOf("section=")){
	var quer=window.location.search;
	var pos=window.location.search.indexOf("section=");
	var tab =quer.substr(pos+8);
	activaTab(tab);
	}

	
});
function activaTab(tab){
    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
};
</script>
<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>

</body>
</html>