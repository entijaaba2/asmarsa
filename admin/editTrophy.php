<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Modifier un titre - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<style>
td{
	padding-top: 15px;
}
</style>
</head> 
   
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$trid=$_GET['id'];
	if (!isset($_GET)) { header('Location: allTrophies.php'); die();}
	else {
		include_once('connect_to_base.php');
		if ($trophy=$bdd->query('SELECT * FROM trophy  as tr, category as cate WHERE tr.cat=cate.cat AND tr.trid="'.$_GET['id'].'"  AND cate.visibility=1')->fetch()); 
		else {
			 header('Location: allTrophies.php'); die();
		} 
	}

	$page="trophy";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="switches">
		<div class=" col-lg-12" >
		<h1 style="text-align: center; margin: 0 auto;">Modifier un titre</h1><br>
		<form class="navbar-form navbar-left" role="search" action="editTrophy2.php" enctype="multipart/form-data" method="post" >

		<div class="col-lg-3 col-lg-offset-2">
		<img src="../img/trophies/<?php echo $trophy['cat'].$trophy['trname'].$trophy['season'];?>.png" height="250">
				  <div class="form-group">
					<br>
				  <h3>Changer la photo</h3>
					<input type="file" name="image" >
				  </div>
		</div>

		<div class="col-lg-6">
		<input type="hidden" value="<?php echo $trophy['trid'];?>" name="trid">
		<div class="form-group" style="padding-left: 180px; ">
					 <select id="category" class="form-control1" style="width : 300px;" name="cat">
						<option value="<?php echo $trophy['cat'];?>"><?php echo $trophy['cat'];?></option>	  
						<?php
								$req=$bdd->query('SELECT * FROM category');
								while($data=$req->fetch()){ 
						?>
							  
							  <option value="<?php echo $data['cat'];?>"><?php echo $data['cat'];?></option>
						<?php } ?>
					 </select> 
				  </div>

      
	  <div style="padding-left: 150px;">
		  <table>
			<tr>
				<td align="center"> Nom du trophée : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="text" name="trname" required class="form-control" value="<?php echo $trophy['trname'];?>">
				  </div>
				</td>
			</tr>
			
			<tr>
				<td align="right"> Saison : &nbsp&nbsp </td>
				<td>
				  <div class="form-group">
					<input type="number" min="1900" max="2100" name="season" required class="form-control" value="<?php echo $trophy['season'];?>">
				  </div>
				</td>
			</tr>
		  

		</table>
		 <br>
		<div id="felsa">
		 <button  class="btn btn-success" style="margin-left: 150px;" type="submit">Changer</button>
		</div>
		</div>
		 </form>
		  

		
		
		</div>
		</div>
		
		
		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
   

 <!-- JS Files -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

</script>


<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>

</body>
</html>