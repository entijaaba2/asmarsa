<div class="left-side sticky-left-side">

			<!--logo and iconic logo start-->
			<div class="logo">
				<h1><a href="index.php"><span>ASM</span></a></h1>
			</div>
			<div class="logo-icon text-center">
				<a href="index.php"><i class="lnr lnr-home"></i> </a>
			</div>

			<!--logo and iconic logo end-->
			<div class="left-side-inner">

				<!--sidebar nav start-->
				<ul class="nav nav-pills nav-stacked custom-nav">
						<li <?php if($page=="index") echo 'class="active"';?>><a href="index.php"><i class="lnr lnr-power-switch"></i><span>Dashboard</span></a></li>
						
						           
						<li class="menu-list"><a href="#"><i class="lnr lnr-dice"></i> <span>Gérer</span></a>
							<ul class="sub-menu-list">
								<li <?php if($page=="account") echo 'class="active"';?>><a href="account.php">Le compte</a> </li>
								<li <?php if($page=="flash") echo 'class="active"';?>><a href="allFlash.php">Flashinfo</a> </li>
								<li <?php if($page=="history") echo 'class="active"';?>><a href="allHistory.php">Histoire et Légende</a> </li>
								<li <?php if($page=="act") echo 'class="active"';?>><a href="allActu.php">L'actualité</a> </li>
								<li <?php if($page=="player") echo 'class="active"';?>><a href="allPlayers.php">Les joueurs</a> </li>		
								<li <?php if($page=="match") echo 'class="active"';?>><a href="allMatchs.php">Les matchs</a> </li>	
								<li <?php if($page=="trophy") echo 'class="active"';?>><a href="allTrophies.php">Palmarès</a> </li>
								<li <?php if($page=="partners") echo 'class="active"';?>><a href="partners.php">Partenaires</a> </li>
								<li <?php if($page=="category") echo 'class="active"';?>><a href="allCategories.php">Sections</a> </li>
								<li <?php if($page=="pole") echo 'class="active"';?>><a href="allPoles.php">Sondage</a> </li>

								<li <?php if($page=="gallery") echo 'class="active"';?>><a href="allAlbums.php">Gallerie</a> </li>

								<li <?php if($page=="staff") echo 'class="active"';?>><a href="allStaff.php">Staff</a> </li>

								<li <?php if($page=="video") echo 'class="active"';?>><a href="video.php">Vidéo</a> </li>
							</ul>
						</li>
						<li <?php if($page=="users") echo 'class="active"';?>><a href="users.php"><i class="lnr lnr-user"></i> <span>Utilisateurs</span></a></li>						
						
					</ul>
				
				<!--sidebar nav end-->
			</div>
		</div>
		