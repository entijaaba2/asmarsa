<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Ajouter un joueur - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<style>
td{
	padding-top: 15px;
}
</style>
</head> 
   
 <body class="sticky-header left-side-collapsed"  onload="initMap()">
    <section>
    <!-- left side start-->
	<?php 
	$pid=$_GET['id'];
	if (!isset($_GET)) { header('Location: allPlayers.php'); die();}
	else {
		include_once('connect_to_base.php');
		if ($player=$bdd->query('SELECT * FROM player as p, category as cate WHERE p.cat=cate.cat AND p.pid="'.$_GET['id'].'"  AND cate.visibility=1')->fetch()); 
		else {
			 header('Location: allPlayers.php'); die();
		} 
	}
	
	
	$page="player";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="switches">
		<div class=" col-lg-12" >
		<h1 style="text-align: center; margin: 0 auto;">Modifier le profil de <?php echo $player['nom']." ".$player['prenom'];?></h1><br>
		<form class="navbar-form navbar-left" role="search" action="editPlayer2.php" enctype="multipart/form-data" method="post" >
		<div class="col-lg-3 col-lg-offset-2">
		<img src="../img/players/<?php echo $player['pid'];?>.jpg">
				  <div class="form-group">
					<br>
				  <h3>Changer la photo</h3>
					<input type="file" name="image" >
				  </div>
		</div>	
		<div class="col-lg-6">
		<div class="form-group" style="padding-left: 180px; ">
					 <select onchange="jerseyCheck();" id="category" class="form-control1" style="width : 300px;" name="cat">
							<option value="<?php echo $player['cat'];?>"><?php echo $player['cat'];?></option>
						<?php
								$req=$bdd->query('SELECT * FROM category WHERE visibility=1');
								while($data=$req->fetch()){ 
						?>
							  
							  <option value="<?php echo $data['cat'];?>"><?php echo $data['cat'];?></option>
						<?php } ?>
					 </select> 
				  </div>
	<input type="hidden" value="<?php echo $player['pid'];?>" name="pid">
      
	  <div style="padding-left: 150px;">
		  <table>
			<tr>
				<td align="right"> Nom : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="text" name="nom" value="<?php echo $player['nom'];?>" class="form-control" placeholder="Nom">
				  </div>
				</td>
			</tr>
			
			<tr>
				<td align="right"> Prénom : &nbsp&nbsp </td>
				<td>
				  <div class="form-group">
					<input type="text" value="<?php echo $player['prenom'];?>" name="prenom" class="form-control" placeholder="Prénom">
				  </div>
				</td>
			</tr>
		  
			<tr>
				<td align="right"> Date de naissance : &nbsp&nbsp </td>
				<td>
				  <div class="form-group">
					<input type="date" value="<?php echo $player['date'];?>" name="date" class="form-control" placeholder="aaaa-mm-jj">
				  </div>
				</td>
			</tr>
			
			
			
			
			<tr>
				<td align="right"> Taille (cm)  : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="number" value="<?php echo $player['height'];?>" step="0.1" name="height" class="form-control" placeholder="Taille">
				  </div>
				</td>
			</tr>
			
			<tr>
				<td align="right"> Poids (kg) : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="number" value="<?php echo $player['weight'];?>" step="0.1" name="weight" class="form-control" placeholder="Poids">
				  </div>
				</td>
			</tr>

			<tr>
				<td align="right"> Numéro maillot : &nbsp&nbsp </td>
				<td>
				  <div id="validateJersey" class="form-group">
					<input type="number" value="<?php echo $player['jersey'];?>" id="jerseynumber" onchange="jerseyCheck();" name="jersey" class="form-control" placeholder="Numéro maillot">
				  </div>
				</td>
			</tr>

			
			
			<tr>
				<td align="right"> Poste : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					 <select  class="form-control1"  name="post">
							  <option value="<?php echo $player['post'];?>"><?php echo $player['post'];?></option>
							  <option value="Gardien">Gardien</option>
							  <option value="Défenseur central">Défenseur central</option>
							  <option value="Arrière droit">Arrière droit</option>
							  <option value="Arrière gauche">Arrière gauche</option>
							  <option value="Milieu défensif">Milieu défensif</option>
							  <option value="Pivot">Pivot</option>
							  <option value="Milieu offensif">Milieu offensif</option>
							  <option value="Aillier">Aillier</option>
							  <option value="Attaquant">Attaquant</option>
					 </select> 
				  </div>
				</td>
			</tr>

		</table>
		 <br>
		<div id="felsa">
		 <button  class="btn btn-success" style="margin-left: 150px;" type="submit">Modifier</button>
		</div>
		</div>
		</div>
		</form>
		  
		  

		
		
		</div>
		</div>
		
		
		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
   

 <!-- JS Files -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});


function jerseyCheck(){

var jersey= document.getElementById('jerseynumber').value;
var category= document.getElementById('category').value;
var pid="<?php echo $pid;?>";

$.ajax({
                        url: "jerseyCheck.php",
                        method:"POST",
                        data: {'jersey': jersey, 'category' : category},
                        success: function(data){  
					
                            switch(data){
							
							case "0" :
								document.getElementById('validateJersey').innerHTML= '<div id="validateJersey" class="form-group has-success">' +
																					 '<input type="number" value="'+jersey+'" id="jerseynumber" onchange="jerseyCheck();" name="jersey" required class="form-control" placeholder="Numéro maillot">' +
																					 '</div>';
								document.getElementById('felsa').innerHTML='<button  class="btn btn-success" style="margin-left: 150px;" type="submit">Modifier</button>';
								
								break;
								
							default :
							{if(parseInt(data)==pid) {
									document.getElementById('validateJersey').innerHTML= '<div id="validateJersey" class="form-group has-success">' +
																					 '<input type="number" value="'+jersey+'" id="jerseynumber" onchange="jerseyCheck();" name="jersey" required class="form-control" placeholder="Numéro maillot">' +
																					 '</div>';
									document.getElementById('felsa').innerHTML='<button  class="btn btn-success" style="margin-left: 150px;" type="submit">Modifier</button>';
								
								break;
								}
								else {
									 document.getElementById('validateJersey').innerHTML= '<div id="validateJersey" class="form-group has-error">' +
																					 '<input type="number" value="'+jersey+'" id="jerseynumber" onchange="jerseyCheck();" name="jersey" required class="form-control" placeholder="Numéro maillot">' +
																					 '</div>';
									 document.getElementById('felsa').innerHTML='<button class="btn btn-success disabled" style="margin-left: 150px;" type="submit">Modifier</button>'
									 break;
								}
								

							}
							}
								
							
                        }
                    });
}
</script>


<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>

</body>
</html>