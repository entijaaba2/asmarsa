<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">

<!--//end-animate-->
<!----webfonts--->
<link href='//fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
<!---//webfonts---> 

<style>
td{
	padding-top: 15px;
}
</style>
</head> 
   
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$page="gallery";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php 
			
	$actu="";
	if(!empty($_GET)){
				
				} else {
					header('Location: allAlbums.php');
					die();
				}
				include("header.php");
				$folder=$_GET['folder'];
				$infos=explode('=',$folder);
				$album=$infos[1];
				$section=$infos[0]
	?>
	<title><?php echo $album;?> - Admin</title>


			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="switches">
		<div class="col-lg-4 col-lg-offset-8">
		<a href="addVideo.php?folder=<?php echo $folder;?>"><button class="btn btn-success">Ajouter vidéos</button></a>
		<a href="gallery.php?folder=<?php echo $folder;?>"><button class="btn btn-success">Modifier</button></a>
		</div>
		<div class=" col-lg-12">
		<h1><?php echo $album;?></h1>
		
			<?php
			$myfile = fopen($folder."/order.txt", "r");
			while(!feof($myfile)) {
				$file=fgets($myfile);
				if($file!=""){
					if ( !strpos($file,"watch?v=") ){
			?>
				<div class="col-lg-4" style="margin : 50px;">
					<img src="<?php echo $folder.'/'.$file;?>">
					<a href="deletePicture.php?picture=<?php echo $folder.'/'.$file;?>"><button  style="margin-top : 50px;" class="btn btn-danger">Supprimer</button></a>
				</div>

			<?php }
			else {
				$code=explode("watch?v=",$file)[1];
				?>
				<div class="col-lg-4" style="margin : 50px;">
				<iframe width="475" height="265" src="https://www.youtube.com/embed/<?php echo $code;?>" frameborder="0" allowfullscreen></iframe><br>
				<a href="deletePicture.php?picture=<?php echo $folder.'/'.$file;?>"><button  style="margin-top : 50px;" class="btn btn-danger">Supprimer</button></a>
				</div>
				<?php
			}
			}} fclose($myfile); ?>
		</div>
		
		</div>
		
		
		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
   
 <!-- JS Files -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

</script>


<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>

</body>
</html>