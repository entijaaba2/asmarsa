<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Gérer le compte - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">

</head> 
   
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$page="partners";
	include("navbar.php");
	$q=0;
	foreach (glob("../img/SponsorLogos/alogo*") as $filename) {
    $q++;
		}
	
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="switches">
		<div class=" col-lg-6">
		
<h2> Liste des partenaires</h2>
<table class="table">
    <thead>
      <tr>
        <th>Logo</th>
        <th style="text-align : center;">Partenaire officiel</th>
        <th style="text-align : right;">Supprimer logo</th>
      </tr>
    </thead>
    <tbody id="#logos">
	<?php
$dir = "../img/SponsorLogos/";
$dh = opendir($dir);
while (($file = readdir($dh)) != false){ 
										if( $file == '.' || $file == '..') continue;

	?>
      <tr>
        <td><img width="30" src="../img/SponsorLogos/<?php echo $file;?>"  alt=""/></td>
        <td style="text-align : center;"><?php if (substr ($file,0,1)=="a") echo '<span class="glyphicon glyphicon-check"></span>';?></td>
        <td style="text-align : right;"><a href="deleteLogo.php?pn=<?php echo $file;?>"><button class="btn btn-danger btn-xs" data-toggle="tooltip" title="supprimer le logo"><i class="fa fa-trash-o " ></i></button></a></td>
      </tr>
	<?php 
										} 
closedir($dh); 
	?>     
    </tbody>
  </table>


		
		</div>
		</div>
		
		<div class=" col-lg-6">
		<h2> Ajouter un partenaire</h2>
		<br>
		<br>
		<form action="uploadLogo.php" method="post" enctype="multipart/form-data">
<input type="file" name="image" style="padding-left : 30%;" >
<br>
<div style="text-align : center;">
 <?php if ($q<3) echo '<input type="checkbox" name="primary" > Partenaire officiel </input>' ; else echo '<input disabled="" type="checkbox"> Partenaire officiel </input><br><div style="color : #d43f3a;">(Vous ne pouvez pas ajoutez plus que 3 partenaires officiels)</div>';?><br>
<br>
<button class="btn btn-success center" type="submit">Upload</button>
</div>
</form>
		</div>
		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
   

 <!-- JS Files -->
<script src="js/jquery.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.min.js"></script>
</body>
</html>