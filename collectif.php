  <!DOCTYPE html>
  <html lang="en">
  <head>
   <title>ASM - Avenir Sportif de la Marsa</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
   <link rel="icon" type="image/png" href="favicons/favicon-32x32.png" sizes="32x32">
   <link rel="icon" type="image/png" href="favicons/favicon-16x16.png" sizes="16x16">
   <link rel="manifest" href="favicons/manifest.json">
   <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
   <meta name="theme-color" content="#ffffff">
   <link rel="stylesheet"  href="css/bootstrap.css">
   <link rel="stylesheet"  href="css/imagehover.css">
   <link rel="stylesheet" href="css/styles.css">
   <link rel="stylesheet" href="css/jquery-ui.css">
   <link rel="stylesheet" href="css/slick.css">
   <link rel="stylesheet" href="css/ionicons.min.css">
   <link rel="stylesheet" href="css/font-awesome.min.css">
   <link rel="stylesheet" href="css/bootstrap-date.css">
   <meta name="robots" content="index,follow,noodp"><!-- All Search Engines -->
   <meta name="googlebot" content="index,follow"><!-- Google Specific -->
   <style type="text/css">
  h1{
    margin-top: 0px !important;
  }
</style>
 </head>
 <body>
  <!-- ******************** NAV *********************** -->
  <?php 
  if(!isset($_GET['section']) || empty($_GET['section'])) {die('You should enter a section !!');}
  include('./nav.php') ;
  $data = $bdd->query('SELECT cat from category WHERE visibility=1')->fetchAll();
  if(!in_array($_GET['section'], array_column($data,'cat'))) { die('No section found !!');}
  $mySection = $_GET['section'];
  ?>
<!-- ******************** SLIDER *********************** -->
<div class="container-fluid main-slider" id="asm">
   <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
      </ol>
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
      <?php 
      $req=$bdd->query('SELECT * FROM actualite WHERE cat="'.$mySection.'" ORDER BY acid DESC LIMIT 8');
      $i=0;
      if($data=$req->fetch()){ $i++; ?>
 <div class="item active">
          <img src="img/actualite/<?php echo $data['acid'];?>.jpg" alt="...">
          <div class="carousel-caption">
            <div class="col-md-12">
              <h1>ASM</h1>
              <h2><?php echo $data['title'];?></h2>
              <span><a href="news.php?id=<?= $data['acid']?>"><button class="btn">VOIR PLUS</button></a></span>
              <div class="row">
                <div class="flex-container">
                  <div class="box"> 
                    <div class="flex-item"><img src="images/white1.png" class="img-responsive"></div>
                    <div class="flex-item"><p>1939</p></div>
                    <div class="flex-item"><img src="images/white2.png" class="img-responsive"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php }
      while($data=$req->fetch()){
        $i++;
        ?>
    
         <div class="item">
          <img src="img/actualite/<?php echo $data['acid'];?>.jpg" alt="...">
          <div class="carousel-caption">
            <div class="col-md-12">
              <h1>ASM</h1>
              <h2><?php echo $data['title'];?></h2>
              <span><a href="news.php?id=<?= $data['acid']?>"><button class="btn">VOIR PLUS</button></a></span>
              <div class="row">
                <div class="flex-container">
                  <div class="box"> 
                    <div class="flex-item"><img src="images/white1.png" class="img-responsive"></div>
                    <div class="flex-item"><p>1939</p></div>
                    <div class="flex-item"><img src="images/white2.png" class="img-responsive"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php if ($i>=4) break;} ?>
    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
   
</div>

<!-- ******************** RESULTATS CLASSEMENT & CALENDRIER *********************** -->
<section class="container-fluid" id="football">
 <div class="col-md-6 resultat-match text-center">
  <div class="header-wrapper center-block text-center">
    <h4>MATCH PREC/SUIV</h4>
  </div>
  <div class="suivi">
  <?php
  $req3=$bdd->query('SELECT *, DATEDIFF(f.date,CURDATE()) as dte FROM fixtures as f, clubs as c WHERE c.clid=f.clid AND c.cat="'.$mySection.'" ORDER BY ABS(dte) ASC LIMIT 2')->fetchAll(); 
  if (count($req3) >= 1){
  foreach($req3 as $data3){
      $score_mar="";
      $score_opp="";
    if($data3['venue']=="Stade Abdelaziz Chtioui") {
      $home=true;
      if ($data3['score']!=""){
      $score_mar=explode("-",$data3['score'])[0];
      $score_opp=explode("-",$data3['score'])[1];
      }
    }
    else{
      $home=false;
       if ($data3['score']!=""){
      $score_mar=explode("-",$data3['score'])[1];
      $score_opp=explode("-",$data3['score'])[0];
    }
    }
  ?>
    <ul class="result-slider">
     <li class="prev-btn"><img src="images/arrow-result.png"></li>
     <li><img src="img/TeamLogos/<?php if($home) echo "ASM"; else echo $data3['cname']; ?>.png" width="107px" class="img-responsive"></li>
     <li><h2><?php if($home) echo $score_mar; else echo $score_opp; ?></h2></li>
     <li><h2>-</h2></li>
     <li><h2><?php if(!$home) echo $score_mar; else echo $score_opp; ?></h2></li>
     <li><img src="img/TeamLogos/<?php if(!$home) echo "ASM"; else echo $data3['cname']; ?>.png" width="107px" class="img-responsive"></li>
     <li class="next-btn"><img src="images/arrow-result.png" class="img-responsive"></li>
    </ul>
   <?php }
    ?>
    <div class="slider-part2">
  <?php foreach($req3 as $data3){ ?>
      <div class="text-center center-block info-match">
      <h3><?php echo $data3['venue']." "; $date = date_create($data3['date']);
echo date_format($date, 'd-m-Y');?></h3>
      <h3><?php echo date_format($date, 'H:i');?></h3>
      </div>
      <?php } } ?>
  
    </div>
  </div>
    <hr></hr>
    <div class="asm-tv">
      <div class="header-wrapper center-block text-center">
        <h4>CALENDRIER</h4>
      </div>
    </div>
  </div>
  <div class="col-md-4 col-md-offset-1 classement">
      <div class="header-wrapper text-center"><h4>CLASSEMENT</h4></div>
      <div class="classement-wrapper">
        <img src="images/classement.jpg" class="img-responsive">
      </div>
    </div>



</section>



<!-- ******************** Effectif *********************** -->

<div class="container-fluid effectif-wrapper">
  <div class="row">
    <div class="tri-gallery text-center center-block">
      <div class="header-wrapper center-block text-center">
        <h4 class="squad-title">EFFECTIF PRO</h4>
      </div>
      <ul class="button-group filter-button-group">
        <li>
          <a href="./squad.php"><button type="button" class="btn btn-squad">
            <div class="corner"><p>EFFECTIF PRO</p></div>
          </button></a>
        </li>
        <li>
          <a href="staff.php"><button type="button" class="btn btn-squad">
            <div class="corner"><p>STAFF PRO</p></div>
          </button></a>
        </li>
        
      </ul>
    </div>
  </div>
</div>

<!-- ******************** HISTOIRE & LEGENDE *********************** -->
<div class="row container-fluid histoire">
  <?php
    $req=$bdd->query('SELECT * FROM history')->fetchAll();
    foreach ($req as $data) {
  ?>
  <div class="">
    
    <div class="col-md-6 col-lg-6 big-img">
    <div class="main-hist-title">
      <h1><?php echo $data['bgtitle'] ; ?></h1>
    </div>
      <img src="img/history/<?php echo $data['hid'] ; ?>.jpg" class="histPic">
      <img src="img/history/calc.png" class="histCalc">
    </div>
    <div class="col-md-6 col-lg-6 histoire-content">
      <div class="header-wrapper-history">
        <h4>HISTOIRE & LEGENDE</h4>
      </div>
      <div><?php echo  html_entity_decode($data['text']) ; ?></div>
      <h2 class="center-block text-center"><?php echo $data['title'] ; ?></h2>
      <ul class="center-block text-center">     
       <li style="display: inline-block ;" class="prev-button"><img class="center-block text-center" src="images/right-arrow.svg"></li>
       <li style="display: inline-block ;" class="next-button"><img class="center-block text-center" height="27px" src="images/right-arrow.svg"></li>
      </ul>   
      </div>
  </div>
  <?php 
  }
  ?>
</div>

<!-- ******************** Palmarès *********************** -->
<?php $req=$bdd->query('SELECT trname,season,count(*) as nb FROM trophy WHERE cat="'.$mySection.'" GROUP BY trname')->fetchAll();?>
<div class="container-fluid palmares-wrapper">
  <div class="row">
    <div class="tri-gallery center-block">
      <div class="header-wrapper center-block text-center">
        <h4 class="palmares-title">PALMARÈS</h4>
      </div>
      <div class="prize-group row" id="prize-slider">
      <?php foreach ($req as $data) { ?>
        <div class=" prize col-xs-<?php echo 12/count($req) ;?>">
          <img class="img-responsive" src="img/trophies/<?php echo $mySection.$data['trname'].$data['season'] ;?>.png">
          <h3><?php echo $data['nb']." ".$data['trname']; if($data['nb']>=2) echo 's' ;?></h3>
        </div>
      <?php } ?>
      </div>
    </div>
  </div>
</div>

<!-- ******************** FOOTER *********************** -->

<?php include('./footer.php'); ?>
<!-- ******************** JS *********************** -->


<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/slider.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/iso.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/custom.js"></script>

</body>
</html>