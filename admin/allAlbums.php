<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Tous les albums - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">

<!--//end-animate-->
<!----webfonts--->
<link href='//fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
<!---//webfonts---> 
 <!-- Meters graphs -->
<script src="js/jquery-1.10.2.min.js"></script>
<!-- Placed js at the end of the document so the pages load faster -->

</head> 
   
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$page="flash";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="col-lg-10">
		<h2>Tous les albums:</h2>
		</div>
		<div class="col-lg-2">
		<a href="gallery.php"><button class="btn btn-success">Ajouter un album</button></a>
		</div>
	
		<div class="switches">
		<div class=" col-lg-12">
		<ul class="nav nav-tabs">
		<?php 
		include_once("connect_to_base.php");
		$req=$bdd->query('SELECT * FROM category  WHERE visibility=1')->fetchAll();
		$activ=false;
		foreach ($req as $data) {
		?>
  <li  <?php if(!$activ) {echo 'class="active"'; $activ=true;}?>><a data-toggle="tab" href="#<?php echo $data['cat'];?>"><?php echo $data['cat'];?></a></li>
		<?php  } ?>
</ul>

<div class="tab-content">
<?php 	$s=false; $i=0;
		foreach ($req as $data0) {
		$i++;	
		?>
  <div id="<?php echo $data0['cat'];?>" class="tab-pane fade <?php if (!$s) {$s=true; echo 'fade in active';}?>">
    <h3><?php echo $data0['cat'];?></h3>
    
	 <table id="example<?php echo $i;?>" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>Titre</th>
                <th>Date de création</th>
                <th>Gérer</th>
                
            </tr>
        </thead>
        
        <tbody>
		<?php
		foreach (glob("../img/gallery/".$data0['cat']."*") as $folder ) {
			
		?>
         <tr>
                <td><a href="album.php?folder=<?php echo $folder;?>"><?php echo explode('=',$folder)[1]; ?></td>
                <td><?php echo date ("F d Y H:i:s.", filemtime($folder)); ?></td></a></td>
                
                <td  style="text-align : center;">
				<a href="deleteAlbum.php?folder=<?php echo $folder;?>" data-toggle="tooltip" title="supprimer l'album"><img src="./images/trash.png"></a>
				<a href="album.php?folder=<?php echo $folder;?>" data-toggle="tooltip" title="modifier l'album"><img src="./images/cog.png"></a>
				</td>
                
            </tr>
		<?php } ?>
		</tbody>
		</table>
		
		
	
  </div>
 <?php  } ?>
</div>
		<br>
     
		</div>
		</div>
		
		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
   

 <!-- JS Files -->
<script src="//code.jquery.com/jquery-1.12.3.js"></script>
 <script src="js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
	var counter="<?php echo $i;?>";
	var tabnb;
	for(var i=0;i<=counter;i++){
		tabnb='#example'+i;
		$(tabnb).DataTable();
	}
	

	
});
function activaTab(tab){
    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
};
</script>
<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>

</body>
</html>