<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Ajouter un flashinfo - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">

<style>
td{
	padding-top: 15px;
}
</style>
</head> 
   <?php
include_once("connect_to_base.php");
if((isset($_POST))&&(!empty($_POST))){
	$time=date("Y-m-d H:i:s");
	$bdd->query('INSERT into flashinfo(cat,title,text,date) VALUES ("'.$_POST['cat'].'", "'.$_POST['title'].'", "'. htmlentities($_POST['text']).'","'.$time.'")');
	header('Location: addFlash.php?r=success');
	die();
} 

?>
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$page="flash";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="switches">
		<div class=" col-lg-12" >
		<h1>Ajouter un flashinfo:</h1><br>
		<form class="navbar-form navbar-left" role="search" action="addFlash.php" method="post" >
		<div class="form-group" style="margin-left : 138px;">
					 <select class="form-control1" style="width : 300px;" name="cat">
							   <option value="Club">Club</option>
						<?php
								$req=$bdd->query('SELECT * FROM category WHERE visibility=1');
								while($data=$req->fetch()){ 
						?>
							  
							  <option value="<?php echo $data['cat'];?>"><?php echo $data['cat'];?></option>
						<?php } ?>
					 </select> 
				  </div>

      
	  <div>
		  <table>
			<tr>
				<td align="left"> Titre du flashinfo : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="text" pattern=".{1,100}" style="width : 300px;" required title="100 caractères au maximum" name="title" required class="form-control" placeholder="Titre">
				  </div>
				</td>
			</tr>
			
			<tr>
				<td align="left"> Texte du flashinfo&nbsp: &nbsp&nbsp </td>
				<td>
				  <div class="form-group">
					<textarea name="text" rows="10" cols="70" pattern=".{1,600}" title="600 caractères au maximum" placeholder="Le texte..."></textarea>
				  </div>
				</td>
			</tr>
		  
			

		</table>
		 <br>
		<div id="felsa">
		 <button  class="btn btn-success" style="margin-left: 150px;" type="submit">Ajouter</button>
		</div>
		</div>
		 </form>
		  

		
		
		</div>
		</div>
		
		
		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
   
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
<!-- Modal content-->
   <div <?php if (!empty($_GET)) {if ($_GET['r']=="success") echo 'class="alert alert-success"'; else echo 'class="alert alert-danger"';}?>>
    <h1 style="text-align :center ;"><?php if (!empty($_GET)) {if ($_GET['r']=="success") echo 'Opération réussie'; else { if ($_GET['r']=="failure") echo 'Echec de l\'opération'; else echo '404'; } } ?></h1> 
<br>
<br>
		<h5 style="text-align :center ;"><?php if (!empty($_GET)) { if ($_GET['r']=="success") echo 'Flashinfo ajouté avec succès!'; else echo 'Une erreur est survenue';}?></h5>
  </div>
   </div>
</div>
 <!-- JS Files -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
function modalShow(){
	if(window.location.search){
	$('#myModal').modal('show');
	}
}
modalShow();

</script>


<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
</body>
</html>