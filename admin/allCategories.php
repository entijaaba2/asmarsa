<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Gérer le compte - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">

<!--//end-animate-->
<!----webfonts--->
<link href='//fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
<!---//webfonts---> 
 <!-- Meters graphs -->
<script src="js/jquery-1.10.2.min.js"></script>
<!-- Placed js at the end of the document so the pages load faster -->

</head> 
   
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$page="category";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="switches">
		<div class=" col-lg-6 col-lg-offset-3">
<div class="col-lg-10">
<h2> Liste des sections de l'ASM</h2>
</div>
<div class="col-lg-2">
		<a href="addCategory.php"><button class="btn btn-success">Ajouter une section</button></a>
		</div>
<table class="table">
    <thead>
      <tr>
        <th style="text-align : left;">Section</th>
        <th style="text-align : right;">Gérer</th>
      </tr>
    </thead>
    <tbody>
	<?php
$req=$bdd->query('SELECT * FROM category');
while ($data=$req->fetch()){ 
		?>
      <tr>
        <td><div style="color : <?php if(!$data['visibility']) echo '#C0C0C0';?>;"><?php echo $data['cat'];?></div></td>
        <td style="text-align : right;">
        <button onclick="openzModal<?php if(!$data['visibility']) echo "2";?>('<?php echo $data['cat'];?>');" <?php if($data['visibility']) echo 'class="btn btn-danger btn-xs" ><i class="fa fa-eye-slash"'; else echo 'class="btn btn-danger btn-xs" style="background-color : #52A6FA; border-color :  #52A6FA" ><i class="fa fa-eye"';?> aria-hidden="true"></i></button>
		<a href="editCategory.php?section=<?php echo $data['cat'];?>"><button style="background-color : #ff6600" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Changer le nom de la section"><i class="fa fa-cog " ></i></button></a>

        </td>
      </tr>
	<?php 
	      }  
	?>     
    </tbody>
  </table>


		
		</div>
		</div>
		
		
		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
   
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
<!-- Modal content-->
   <div class="alert alert-danger">
    <h1 style="text-align :center ;">ATTENTION </h1> 
<br>
<br>
		<h5 style="text-align :center ;">Vous allez cacher une section ! Pour y-procèder veuillez cliquer sur boutton</h5>
		<br>
		<div style="text-align :center ;" id="felsetDelete">
		
		</div>
  </div>
   </div>
</div>

<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">
<!-- Modal content-->
   <div class="alert alert-success">
    <h1 style="text-align :center ;">ATTENTION </h1> 
<br>
<br>
		<h5 style="text-align :center ;">Vous allez réafficher une section ! Pour y-procèder veuillez cliquer sur boutton</h5>
		<br>
		<div style="text-align :center ;" id="felsetShow">
		
		</div>
  </div>
   </div>
</div>
 <!-- JS Files -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();

});

function openzModal(category){
	document.getElementById('felsetDelete').innerHTML='<a href="hideCategory.php?section='+category+'" ><button  class="btn btn-danger btn-xs" data-toggle="tooltip" title="Cacher la section"><i class="fa fa-eye" ></i> Cacher</button></a>';
	$('#myModal').modal('show');

	
}

function openzModal2(category){
	document.getElementById('felsetShow').innerHTML='<a href="showCategory.php?section='+category+'" ><button  class="btn btn-success btn-xs" style="background : #5cb85c;" data-toggle="tooltip" title="Afficher la section"><i class="fa fa-eye"  ></i> Afficher</button></a>';
	$('#myModal2').modal('show');

	
}

</script>
<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
</body>
</html>