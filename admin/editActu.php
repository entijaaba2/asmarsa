<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Modifier une actualité - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<style>
td{
	padding-top: 15px;
}
</style>
</head> 
   <?php
include_once("connect_to_base.php");
$actu="";
if((isset($_POST))&&(!empty($_POST))){
	$bdd->query('UPDATE actualite SET title="'.$_POST['title'].'", cat="'.$_POST['cat'].'", article="'.htmlentities($_POST['article']).'"   WHERE acid="'.$_GET['id'].'"');
	header('Location: allActu.php?r=success');
	die();
} 
if(isset($_GET)){

				if($actu=$bdd->query('SELECT * FROM actualite as act, category as cate WHERE act.cat=cate.cat AND act.acid="'.$_GET['id'].'"  AND cate.visibility=1')->fetch());
				else {
					header('Location: allActu.php');
					die();
				}
				
				} else {
					header('Location: allActu.php');
					die();
				}


?>
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$page="act";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="switches">
		<div class=" col-lg-12" >
		<div class=" col-lg-10" >
		<h1>Modifier une actualité :</h1><br>
		</div>
		<div class="col-lg-2">
		<a href="allActu.php"><button class="btn btn-success">Voir toutes les actualités</button></a>
		</div>
		<form class="navbar-form navbar-left" role="search" action="editActu.php?id=<?php echo $_GET['id'];?>" enctype="multipart/form-data" method="post" >
		<div class="form-group" style="margin-left : 60px;">
					 <select class="form-control1" style="width : 300px;" name="cat">
						 <option value="<?php echo $actu['cat'];?>"><?php echo $actu['cat'];?></option>	  
						<?php
								$req=$bdd->query('SELECT * FROM category  WHERE visibility=1');
								while($data=$req->fetch()){ 
						?>
							  
							  <option value="<?php echo $data['cat'];?>"><?php echo $data['cat'];?></option>
						<?php } ?>
					 </select> 
				  </div>

      
	  <div>
		  <table>
			<tr>
				<td align="left"> Titre : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="text" pattern=".{1,100}" style="width : 300px;" required title="100 caractères au maximum" name="title" required class="form-control" value="<?php echo $actu['title'];?>">
				  </div>
				</td>
			</tr>
			
			<tr>
				<td align="left"> Article&nbsp: &nbsp&nbsp </td>
				<td>
				  <div class="form-group">
					<textarea name="article" rows="10" cols="70" value="<?php echo $actu['article'];?>"><?php echo $actu['article'];?></textarea>
				  </div>
				</td>
			</tr>
		  
			

		</table>
		 <br>
		<div id="felsa">
		 <button  class="btn btn-success" style="margin-left: 150px;" type="submit">Modifier</button>
		</div>
		</div>
		 </form>
		  

		
		
		</div>
		</div>
		
		
		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  
 <!-- JS Files -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
function modalShow(){
	if(window.location.search){
	$('#myModal').modal('show');
	}
}
modalShow();

</script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>

<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>

</body>
</html>