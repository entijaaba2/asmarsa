<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Ajouter album - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">

<link href="css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />

<style>
.input-group {
	margin: 0 0 0 0;
}
</style>


</head> 
   
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$page="gallery";
	include("navbar.php");
	$getatt=false;
	if ( (isset($_GET['folder'])) && (!empty($_GET['folder'])) ) {
		$getatt=true;
		if(!file_exists($_GET['folder'].'/')) {
			header('Location: index.php');
		}
		$tab=explode('=',$_GET['folder']);
		$section=$tab[0];
		$album=$tab[1];
	}
	
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
			<?php if(!$getatt){ ?>
				<select class="form-control" id="section">
					<option value="" disabled selected>Choisissez la section correspondante</option>
				  <?php 
					include_once("connect_to_base.php");
					$req=$bdd->query('SELECT * FROM category WHERE visibility=1')->fetchAll();
					foreach ($req as $data) {
					?>
					<option value="<?php echo $data['cat'] ; ?>"><?php echo $data['cat'] ; ?></option> <?php } ?>
				  
				</select>
			<?php 
		} else echo '<input id="section" value="'.$section.'" type="hidden">'; 

			?>
				<br>

			<?php if(!$getatt) { ?>
				<input type="text" name="folder" placeholder="Nom de l'album" id="folder" hidden style="width : 100%;">

			<?php } else echo '<input type="hidden" id="folder" name="folder" value="../img/gallery/'.$_GET['folder'].'">'; ?>
				<div id="img-upload" hidden>
						<input id="input-44" name="input44[]" type="file" multiple class="file-loading">
						<div id="errorBlock" class="help-block"></div></div>
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  


 <!-- JS Files -->
<script src="../assets/js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/canvas-to-blob.min.js" type="text/javascript"></script>
<!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview.
     This must be loaded before fileinput.min.js -->
<script src="js/sortable.min.js" type="text/javascript"></script>
<!-- purify.min.js is only needed if you wish to purify HTML content in your preview for HTML files.
     This must be loaded before fileinput.min.js -->
<script src="js/purify.min.js" type="text/javascript"></script>
<!-- the main fileinput plugin file -->
<script src="js/fileinput.min.js"></script>


<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
<script>
$(document).on("ready", function() {

	var sec = "" ;
	if (window.location.search) {
		$("#img-upload").show();
		sec = $("#section").val() ;
		fol = $("#folder").val() ;
	}
	else 
	$("#section").change(function() {
		$("#folder").show();
		$("#folder").change(function() {
		$("#img-upload").show();
		sec = $("#section").val() ;
		fol = $("#folder").val() ;
	});
	});


    $("#input-44").fileinput({
        uploadAsync: false,
        uploadUrl: "upload.php", // your upload server url
        uploadExtraData: function() {
        	return {
        		section: sec,
        		folder : fol
        	} ;
        }
       
    });
});
</script>
   
</body>
</html>