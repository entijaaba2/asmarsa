<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Modifier une Histoire&Légende - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<style>
td{
	padding-top: 15px;
}
</style>
</head> 
   <?php
include_once("connect_to_base.php");
$actu="";
if((isset($_POST))&&(!empty($_POST))){
	include('replace_accented.php');
	$bgtitle=strtoupper(strtr($_POST['bgtitle'], $unwanted_array ));
	$title=strtoupper(strtr($_POST['title'], $unwanted_array ));
	$bdd->query('UPDATE history SET title="'.$title.'", bgtitle="'.$bgtitle.'", cat="'.$_POST['cat'].'", text="'.htmlentities($_POST['text']).'"   WHERE hid="'.$_GET['id'].'"');
	if (!empty($_FILES['image']['tmp_name'])) move_uploaded_file($_FILES['image']['tmp_name'], "../img/history/".$_GET['id'].".jpg");
	header('Location: allHistory.php');
	die();
} 
if(isset($_GET)){

				if($hist=$bdd->query('SELECT * FROM history  as his, category as cate WHERE his.cat=cate.cat AND his.hid="'.$_GET['id'].'"  AND cate.visibility=1')->fetch());
				else {
					header('Location: allHistory.php');
					die();
				}
				
				} else {
					header('Location: allHistory.php');
					die();
				}


?>
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$page="history";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="switches">
		<div class=" col-lg-12" >
		<div class=" col-lg-10" align="center">
		<h1>Modifier une Histoire&Légende :</h1><br>
		</div>
		<div class="col-lg-2">
		<a href="allActu.php"><button class="btn btn-success">Voir toutes les Histoires&Légendes</button></a>
		</div>

		<form class="navbar-form navbar-left" role="search" action="editHistory.php?id=<?php echo $_GET['id'];?>" enctype="multipart/form-data" method="post" >

		<div class="form-group" style="margin-left : 106px;">
					 <select class="form-control1" style="width : 300px;" name="cat">
						 <option value="<?php echo $hist['cat'];?>"><?php echo $hist['cat'];?></option>	  
						<?php
								$req=$bdd->query('SELECT * FROM category');
								while($data=$req->fetch()){ 
						?>
							  
							  <option value="<?php echo $data['cat'];?>"><?php echo $data['cat'];?></option>
						<?php } ?>
					 </select> 
				  </div>

      <div class="col-lg-5">
      <div class="col-lg-12">
      <img src="../img/history/<?php echo $hist['hid'];?>.jpg" width="440">
      </div>
				  <div class="form-group">
					<br>
				  <h3>Changer la photo</h3>
					<input type="file" name="image" >
				  </div>
      </div>

	  <div class="col-lg-7">
		  <table>
			<tr>
				<td align="left"> Titre : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="text" style="width : 300px;" required name="title" required class="form-control" value="<?php echo $hist['title'];?>">
				  </div>
				</td>
			</tr>

			<tr>
				<td align="left"> Grand titre : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="text" pattern=".{1,10}" style="width : 300px;" required title="10 caractères au maximum" name="bgtitle" required class="form-control" value="<?php echo $hist['bgtitle'];?>">
				  </div>
				</td>
			</tr>


			
			<tr>
				<td align="left"> Texte&nbsp: &nbsp&nbsp </td>
				<td>
				  <div class="form-group">
					<textarea name="text" rows="10" cols="70" value="<?php echo $hist['text'];?>"><?php echo $hist['text'];?></textarea>
				  </div>
				</td>
			</tr>
		  
			

		</table>
		 <br>
		<div id="felsa">
		 <button  class="btn btn-success" style="margin-left: 150px;" type="submit">Modifier</button>
		</div>
		</div>
		 </form>
		  

		
		
		</div>
		</div>
		
		
		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  
 <!-- JS Files -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

</script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>

<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>

</body>
</html>