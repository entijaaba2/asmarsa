  <!DOCTYPE html>
  <html lang="en">
  <head>
   <title>ASM - Avenir Sportif de la Marsa</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
   <link rel="icon" type="image/png" href="favicons/favicon-32x32.png" sizes="32x32">
   <link rel="icon" type="image/png" href="favicons/favicon-16x16.png" sizes="16x16">
   <link rel="manifest" href="favicons/manifest.json">
   <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
   <meta name="theme-color" content="#ffffff">
   <link rel="stylesheet"  href="css/bootstrap.css">
   <link rel="stylesheet"  href="css/imagehover.css">
   <link rel="stylesheet" href="css/styles.css">
   <link rel="stylesheet" href="css/slick.css">
   <link rel="stylesheet" href="css/jquery-ui.css">
   <link rel="stylesheet" href="css/ionicons.min.css">
   <link rel="stylesheet" href="css/font-awesome.min.css">
   <link rel="stylesheet" href="css/intro.css">
   <link rel="stylesheet" href="css/photobox.css">
   <meta name="robots" content="index,follow,noodp"><!-- All Search Engines -->
   <meta name="googlebot" content="index,follow"><!-- Google Specific -->
 </head>
 <body>
<!-- ******************** INTRO *********************** -->
 <div id="loader-wrapper"> 
    <div class="loader-section section-left">
      <div class="section-up">
        <div class="filter-up"></div>
        <div class="btn-holder">
          <button>REJOIGNEZ <strong>EL GNEWIA</strong> <i class="ionicons ion-social-instagram-outline"><!----></i> </button>
          <h4>INSTAGRAM.COM/ASM-MARSA</h4>
        </div>
      </div>
      <div class="section-down">
        <div class="filter-down"></div>
        <div class="btn-holder">
          <button>REJOIGNEZ <strong>EL GNEWIA</strong> <i class="ionicons ion-social-youtube"><!----></i> </button>
          <h4>YOUTUBE.COM/ASM-MARSA</h4>
        </div>
      </div>
    </div>
    <div class="loader-section section-middle">
      <div class="filter-middle"></div>
      <div class="btn-holder">
        <button>REJOIGNEZ <strong>EL GNEWIA</strong> <i class="ionicons ion-social-facebook"><!----></i> </button>
        <h4>FACEBOOK.COM/ASMCAFE</h4>
      </div>
    </div>
    <div class="loader-section section-right">
      <div class="filter-right"></div>
      <div class="logo-holder">
        <img class="img-responsive" src="images/logo_asm.png">
        <h3>AVENIR SPORTIF DE LA MARSA</h3>
      </div>
      <div class="btn-holder">
        <button id="visit-btn">VISITEZ LE SITE DIRECTEMENT</button>
        <h4>AS-MARSA.COM</h4>
      </div>
    </div>
 
</div>

 <!-- ******************** NAV *********************** -->
 <?php include('nav.php');?>

<!-- ******************** SLIDER *********************** -->
<div class="container-fluid main-slider" id="asm">
   <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
      </ol>
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
      <?php 
      $req=$bdd->query('SELECT * FROM actualite ORDER BY acid DESC LIMIT 8');
      $i=0;
      if($data=$req->fetch()){ $i++; ?>
 <div class="item active">
          <img src="img/actualite/<?php echo $data['acid'];?>.jpg" alt="...">
          <div class="carousel-caption">
            <div class="col-md-12">
              <h1>ASM</h1>
              <h2><?php echo $data['title'];?></h2>
              <span><a href="news.php?id=<?= $data['acid']?>"><button class="btn">VOIR PLUS</button></a></span>
              <div class="row">
                <div class="flex-container">
                  <div class="box"> 
                    <div class="flex-item"><img src="images/white1.png" class="img-responsive"></div>
                    <div class="flex-item"><p>1939</p></div>
                    <div class="flex-item"><img src="images/white2.png" class="img-responsive"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php }
      while($data=$req->fetch()){
        $i++;
        ?>
    
         <div class="item">
          <img src="img/actualite/<?php echo $data['acid'];?>.jpg" alt="...">
          <div class="carousel-caption">
            <div class="col-md-12">
              <h1>ASM</h1>
              <h2><?php echo $data['title'];?></h2>
              <span><a href="news.php?id=<?= $data['acid']?>"><button class="btn">VOIR PLUS</button></a></span>
              <div class="row">
                <div class="flex-container">
                  <div class="box"> 
                    <div class="flex-item"><img src="images/white1.png" class="img-responsive"></div>
                    <div class="flex-item"><p>1939</p></div>
                    <div class="flex-item"><img src="images/white2.png" class="img-responsive"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php if ($i>=4) break;} ?>
    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
   
</div>

<!-- ******************** RESULTAT & FLASH INFO *********************** -->
<divclass="container-fluid result-section" id="football">
 <div class="col-md-6 resultat-match text-center">
  <div class="header-wrapper center-block text-center">
    <h4>MATCH PREC/SUIV</h4>
  </div>
  <div class="suivi">
  <?php 
  $req3=$bdd->query('SELECT *, ABS(DATEDIFF(f.date,CURDATE())) as dte FROM fixtures as f, clubs as c WHERE c.clid=f.clid ORDER BY dte ASC LIMIT 2')->fetchAll();
  if (($req3[1]['score']=="")&&($req3[0]['score']!="")){
    $tmp=$req3[0];
    $req3[0]=$req3[1];
    $req3[1]=$tmp;
  }
  foreach($req3 as $data3){
      $score_mar="";
      $score_opp="";
    if($data3['venue']=="Stade Abdelaziz Chtioui") {
      $home=true;
      if ($data3['score']!=""){
      $score_mar=explode("-",$data3['score'])[0];
      $score_opp=explode("-",$data3['score'])[1];
      }
    }
    else{
      $home=false;
       if ($data3['score']!=""){
      $score_mar=explode("-",$data3['score'])[1];
      $score_opp=explode("-",$data3['score'])[0];
    }
    }
  ?>
    <ul class="result-slider">
     <li class="prev-btn"><img src="images/arrow-result.png"></li>
     <li><img src="img/TeamLogos/<?php if($home) echo "ASM"; else echo $data3['cname']; ?>.png" width="107px" class="img-responsive"></li>
     <li><h2><?php if($home) echo $score_mar; else echo $score_opp; ?></h2></li>
     <li><h2>-</h2></li>
     <li><h2><?php if(!$home) echo $score_mar; else echo $score_opp; ?></h2></li>
     <li><img src="img/TeamLogos/<?php if(!$home) echo "ASM"; else echo $data3['cname']; ?>.png" width="107px" class="img-responsive"></li>
     <li class="next-btn"><img src="images/arrow-result.png" class="img-responsive"></li>
    </ul>
   <?php }
    ?>
    <div class="slider-part2">
  <?php foreach($req3 as $data3){ ?>
      <div class="text-center center-block info-match">
      <h3><?php echo $data3['venue']." "; $date = date_create($data3['date']);
echo date_format($date, 'd-m-Y');?></h3>
      <h3><?php echo date_format($date, 'H:i');?></h3>
      </div>
      <?php } ?>
  
    </div>
  </div>
    <hr></hr>
    <div class="asm-tv">
      <iframe width="90%" height="350px"src="https://www.youtube.com/embed/<?php echo $bdd->query('SELECT link FROM video ORDER BY vid DESC')->fetch()[0];?>" frameborder="0" allowfullscreen></iframe>
      <p>ASM TV</p>
    </div>
  </div>
</div>

<section class="col-md-6 flash-info">
    <div class="header-wrapper-flash">
      <h4>FLASH INFO</h4>
    </div>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <?php 
    $req2=$bdd->query('SELECT * FROM flashinfo as f ORDER BY flid DESC LIMIT 10');
    if($data2=$req2->fetch()){
    ?>
    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          <?php echo $data2['title'];?> 
          <small class="pull-right">  
          <?php 
           $dStart = new DateTime($data2['date']);
           $dEnd  = new DateTime(date("Y-m-d H:i:s"));
           $dDiff = $dStart->diff($dEnd);
           $minutes= $dDiff->format("%I");
           $hours= $dDiff->format("%H");
           $days= $dDiff->days;
           if (intval($days)==0) {
            if (intval($hours)==0) {
              echo intval($minutes)." min";
           }
            else echo intval($hours)." heure(s)";
           }
            else echo intval($days)." jour(s)";
           ?>
           </small>
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <?php echo html_entity_decode($data2['text']);?>
        <div class="pull-right flash-social">
          <ul>
            <li><a href=""><i class="fa fa-facebook"></i></a></li>
            <li><a href=""><i class="fa fa-twitter"></i></a></li>
            <li><a href=""><i class="fa fa-youtube-square"></i></a></li>
            <li><a href=""><i class="fa fa-instagram"></i></a></li>
          </ul>
        </div>
      </div>
    </div> 
    </div>

    <?php 
      } 
    $i=1;
    include("dictionary.php");
    while($data2=$req2->fetch()){
      $i++;
    ?>

    <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading<?php echo $dictionary[$i];?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $dictionary[$i];?>" aria-expanded="false" aria-controls="collapse<?php echo $dictionary[$i];?>">
      <h4 class="panel-title">
        <a  class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $dictionary[$i];?>" aria-expanded="false" aria-controls="collapse<?php echo $dictionary[$i];?>">
          <?php echo $data2['title'];?> 
          <small class="pull-right">  
          <?php 
           $dStart = new DateTime($data2['date']);
           $dEnd  = new DateTime(date("Y-m-d H:i:s"));
           $dDiff = $dStart->diff($dEnd);
           $minutes= $dDiff->format("%I");
           $hours= $dDiff->format("%H");
           $days= $dDiff->days;
           if (intval($days)==0) {
            if (intval($hours)==0) {
              echo intval($minutes)." min";
           }
            else echo intval($hours)." heure(s)";
           }
            else echo intval($days)." jour(s)";
           ?>
           </small>
        </a>
      </h4>
    </div>
    <div id="collapse<?php echo $dictionary[$i];?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $dictionary[$i];?>">
      <div class="panel-body">
        <?php echo html_entity_decode($data2['text']);?>
        <div class="pull-right flash-social">
          <ul>
            <li><a href=""><i class="fa fa-facebook"></i></a></li>
            <li><a href=""><i class="fa fa-twitter"></i></a></li>
            <li><a href=""><i class="fa fa-youtube-square"></i></a></li>
            <li><a href=""><i class="fa fa-instagram"></i></a></li>
          </ul>
        </div>
      </div>
    </div> 
    </div>
    <?php } ?>

</div>
</section>




<!-- ******************** Actualites II *********************** -->

<div class="container-fluid gal-wrapper">
  <div class="row gal-act2">
    <div class="header-wrapper center-block text-center">
        <h4>Actualités</h4>
      </div>
      <?php 
      while($data=$req->fetch()){ ?>
    <div class="col-md-6">
      <figure class="imghvr-reveal-up">
        <img src="img/actualite/<?php echo $data['acid'];?>.jpg" class="img-responsive" width="200%">
        <figcaption>
          <?php echo $data['title'];?>
        </figcaption>
      </figure>
    </div>
    <?php } ?>
  </div>
  
</div>
<!-- ******************** Classement *********************** -->
<div class="container-fluid classement-section">
  <div class="row">
    <div class="col-md-5 col-lg-offset-1 sondage-wrapper ">
      <div class="header-wrapper text-center center-block"><h4>SONDAGE</h4></div>
      <div class="sondage-content text-center ">
      <?php $pole=$bdd->query('SELECT * FROM sondage ORDER BY sid DESC LIMIT 1')->fetch();?>
      <h5 class="text-center center-block"> <?php echo $pole['question'];?></h5>
      <hr>
      <div id="poleContent">
      <label class="radio-inline">
          <input name="radioGroup" id="radio1" value="<?php echo $pole['fanswer'];?>" type="radio"><?php echo $pole['fanswer'];?>
      </label>
      <div id="firstAnswer"><br></div>
      <label class="radio-inline">
          <input name="radioGroup" id="radio2" value="<?php echo $pole['sanswer'];?>" type="radio"> <?php echo $pole['sanswer'];?>
        </label>
      <div id="secondAnswer"><br></div>
      <button type="button" class="btn btn-warning text-center center-block" onclick="answerPole(<?php echo $pole['sid'];?>);">Vote</button>
      </div>
      </div>
    </div>
    <div class="col-md-5 classement center-block">
      <div class="header-wrapper text-center center-block "><h4>CLASSEMENT</h4></div>
      <div class="classement-wrapper text-center center-block">
        <img src="images/classement.jpg" class="img-responsive center-block classement-img">
      </div>
    </div>
  </div>
</div>
<!-- ******************** GALLERY *********************** -->
<div class="container-fluid gallery">
  <div class="row">
    <div class="tri-gallery text-center center-block">
      <div class="header-wrapper center-block text-center">
        <h4>GALERIE</h4>
      </div>
      <ul class="button-group filter-button-group">
      <li>
          <button id="allB" type="button" class="btn btn-warning" data-filter="*">All</button>
        </li>
        <?php
          $req=$bdd->query('SELECT * FROM category')->fetchAll();
          foreach ($req as $data) { 
        ?>
        <li>
          <button type="button" class="btn btn-warning" data-filter=".<?php echo $data['cat'] ; ?>"><?php echo $data['cat'] ; ?></button>
        </li>
        <?php } ?>
      </ul>
    </div>
    <div class="grid">
    <div class="col-md-4 col-xs-4 foot myvid">
    <a href="https://www.youtube.com/embed/MKqghyQTArI" rel="video">
      <figure class="imghvr-fade">
        <img src="images/actu2_1.jpg" class="img-responsive">
        <figcaption class="gallery-hover">
          <i class="ionicons ion-play"></i>
        </figcaption>
      </figure>
    </a>
  </div>
  <?php 
  $rootcount =0;
  foreach (glob("img/gallery/*",GLOB_ONLYDIR) as $folder) {
    $fname = explode("/", $folder)[2] ;
    $fname = explode("=", $fname)[0];
    $rootcount++;
    if($rootcount==9) break; ?>
    <div class="col-md-4 col-xs-4 bba <?php echo $fname ; ?>">
      <?php $folderCount = 0;
      $all = glob($folder."/*.*");
      $txt = glob($folder."/*.txt");
      $diffA = array_diff($all, $txt);
      usort($diffA, create_function('$a,$b', 'return filemtime($a) - filemtime($b);'));
      foreach ($diffA as $filename) { 
        $fullname = explode("/", $filename)[2] ;
        $name = explode("=", $fullname)[0] ;
        if(!$folderCount) {
          ?>
          <a href="<?php echo $filename ; ?>">
            <figure class="imghvr-fade">
              <img src="<?php echo $filename ; ?>" class="img-responsive gallery">
              <figcaption class="gallery-hover">
                <i class="fa fa-search-plus"></i>
              </figcaption>
            </figure>
          </a>
          
          <?php 
        }
        else { ?>
        <a href="<?php echo $filename ; ?>" class="sr-only" style="display: none;" hidden>
          <figure class="imghvr-fade">
            <img src="<?php echo $filename ; ?>" class="img-responsive gallery">
            <figcaption class="gallery-hover">
              <i class="fa fa-search-plus"></i>
            </figcaption>
          </figure>
        </a>
        <?php }
        $folderCount++;
      } ?>
    </div>
    <?php } ?>
  </div>
</div>
</div>


<!-- ******************** HISTOIRE & LEGENDE *********************** -->
<div class="row container-fluid histoire">
  <?php
    $req=$bdd->query('SELECT * FROM history')->fetchAll();
    foreach ($req as $data) {
  ?>
  <div class="">
    
    <div class="col-md-6 col-lg-6 big-img">
    <div class="main-hist-title">
      <h1><?php echo $data['bgtitle'] ; ?></h1>
    </div>
      <img src="img/history/<?php echo $data['hid'] ; ?>.jpg" class="histPic">
      <img src="img/history/calc.png" class="histCalc">
    </div>
    <div class="col-md-6 col-lg-6 histoire-content">
      <div class="header-wrapper-history">
        <h4>HISTOIRE & LEGENDE</h4>
      </div>
      <div><?php echo  html_entity_decode($data['text']) ; ?></div>
      <h2 class="center-block text-center"><?php echo $data['title'] ; ?></h2>
      <ul class="center-block text-center">     
       <li style="display: inline-block ;" class="prev-button"><img class="center-block text-center" src="images/right-arrow.svg"></li>
       <li style="display: inline-block ;" class="next-button"><img class="center-block text-center" height="27px" src="images/right-arrow.svg"></li>
      </ul>   
      </div>
  </div>
  <?php 
  }
  ?>
</div>

<!-- ******************** FOOTER *********************** -->
<?php include('footer.php');?>
<!-- ******************** JS *********************** -->


<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/slider.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/iso.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/custom.js"></script>
<script src="js/intro.js"></script>
<script src="js/jquery.photobox.js"></script>
<script>$('.bba').photobox('a',{ time:0 });</script>
<script>$('.myvid').photobox('a',{ time:0 });</script>
<style type="text/css">
  h1{
    margin-top: 0px !important;
  }
</style>
</body>
</html>