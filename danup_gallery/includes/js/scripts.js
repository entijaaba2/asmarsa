
function checkWidth(init) {

  if ($(window).width() <= 480) {
    $('.p-right img').addClass('img-responsive');
  }else {
  	$('.p-right img').removeClass('img-responsive');
  } 
}

$(document).ready(function() {
  checkWidth(true);

  $(window).resize(function() {
    checkWidth(false);
  });
});




$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});


$(".dropdown-toggle").click(function(){
    $(this).attr("aria-expanded","false");
});


$(document).ready(function(){

$(function(){
 
    $(document).on( 'scroll', function(){
 
      if ($(window).scrollTop() > 100) {
      $('.scroll-top-wrapper').addClass('show');
    } else {
      $('.scroll-top-wrapper').removeClass('show');
    }
  });
 
  $('.scroll-top-wrapper').on('click', scrollToTop);
});
 
function scrollToTop() {
  verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
  element = $('body');
  offset = element.offset();
  offsetTop = offset.top;
  $('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
}

});

// --------------- Parallax Scripts ---------------- //

var currentX = '';
var currentY = '';
var movementConstant = .015;
$(document).mousemove(function(e) {
  if(currentX == '') currentX = e.pageX;
  var xdiff = e.pageX - currentX;
  currentX = e.pageX;
   if(currentY == '') currentY = e.pageY;
  var ydiff = e.pageY - currentY;
  currentY = e.pageY; 
  $('.parallax div').each(function(i, el) {
      var movement = (i + 1) * (xdiff * movementConstant);
    var movementy = (i + 1) * (ydiff * movementConstant);
      var newX = $(el).position().left + movement;
    var newY = $(el).position().top + movementy;
      $(el).css('left', newX + 'px');
    $(el).css('top', newY + 'px');
  });
});



$(document).ready(function(){
  $('.parallax').mousemove(function(e){
    var x = -(e.pageX + this.offsetLeft) / 100;
    var y = -(e.pageY + this.offsetTop) / 100;
    $(this).css('background-position', x + 'px ' + y + 'px', 2000, 'easeInExpo');
  });    
});


