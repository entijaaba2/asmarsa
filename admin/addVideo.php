<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Ajouter des vidéos - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">

<style>
td{
	padding-top: 15px;
}
</style>
</head> 
   <?php
   
$folder=$_GET['folder'];
$alb=explode("/",$folder)[3];
$album=explode("=",($alb))[1];
include_once("connect_to_base.php");
if((isset($_POST))&&(!empty($_POST))){
	$file = $folder."/order.txt";
$current = file_get_contents($file);
$current .= $_POST['links'].PHP_EOL;
file_put_contents($file, $current);
	header('Location: album.php?folder='.$folder);
	die();
} 

?>
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$page="gallery";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="switches">
		<div class=" col-lg-12" >
		<h1>Ajouter des vidéos à l'album <?php echo $album; ?>:</h1><br>
		<form class="navbar-form navbar-left" role="search" action="addVideo.php?folder=<?php echo $folder; ?>" method="post" >
		

      
		  <table>
			<tr>
				<td align="left"> Ajouter les liens YouTube des vidéos : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
				  <textarea name="links" cols="30" rows="10"></textarea>
				  </div>
				</td>
			</tr>
			Pour ajouter plusieurs vidéos, il suffit d'ajouter un retour à la line après chaque lien.
			

		</table>
		 <br>
		<div id="felsa">
		 <button  class="btn btn-success" style="margin-left: 150px;" type="submit">Ajouter</button>
		</div>
		</div>
		 </form>
		  

		
		
		</div>
		</div>
		
		
		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
   

 <!-- JS Files -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

</script>


<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
</body>
</html>