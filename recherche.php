  <!DOCTYPE html>
  <html lang="en">
  <head>
 <?php if ((isset($_GET)) && (!empty($_GET))){
  $query=$_GET['query'];
  }
  else {
  header('Location: index.php');
  die();
  }?>
  <title>Résultat recherche - <?php echo $query;?></title>
  <meta charset="utf-8-bom">
   <link href="assets/css/bootstrap.css" rel="stylesheet">
   <link href="css/styles.css" rel="stylesheet">
   <link rel="stylesheet"  href="css/bootstrap.css">
   <link rel="stylesheet" href="css/styles.css">
   <link rel="stylesheet" href="css/styles-squad.css">
   <link rel="stylesheet" href="css/ionicons.min.css">
   <link rel="stylesheet" href="css/jquery-ui.css">
   <link rel="stylesheet" href="css/font-awesome.min.css">
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
   <link rel="icon" type="image/png" href="favicons/favicon-32x32.png" sizes="32x32">
   <link rel="icon" type="image/png" href="favicons/favicon-16x16.png" sizes="16x16">
<style type="text/css">
div.ui-widget-content{
  border : 0px ;
}
.back-title{
  font-size: 1.6em !important;
}
.back p {
  bottom: 40px !important;
}
.notfound{
  padding : 18vh 40vw;
  font-size: 2.2em;
}
</style>
 </head>
 <body>

  <!-- ******************** NAV *********************** -->
  <?php 
  include_once('connect_to_base.php');
  include('nav_lin.php');?>

<!-- ******************** SQUAD *********************** -->
<?php 
 include_once("connect_to_base.php") ;
$act=$bdd->query("    SELECT *
            FROM actualite
            WHERE title LIKE '%".$query."%' OR article LIKE '%".$query."%' OR cat  LIKE '%".$query."%'")->fetchAll();

$his=$bdd->query("    SELECT *
            FROM history 
            WHERE title LIKE '%".$query."%' OR text LIKE '%".$query."%'  OR cat  LIKE '%".$query."%'")->fetchAll();

$flash=$bdd->query("  SELECT *
            FROM flashinfo 
            WHERE title LIKE '%".$query."%' OR text LIKE '%".$query."%' OR cat  LIKE '%".$query."%'")->fetchAll();

$fixtures=$bdd->query("   SELECT *
                FROM fixtures as f, clubs as c 
                WHERE venue LIKE '%".$query."%' OR date LIKE '%".$query."%' OR c.cat  LIKE '%".$query."%' OR score  LIKE '%".$query."%'  OR c.cname  LIKE '%".$query."%' AND f.clid=c.clid")->fetchAll();

$trophy=$bdd->query(" SELECT *
            FROM trophy 
            WHERE trname LIKE '%".$query."%' OR season LIKE '%".$query."%' OR cat  LIKE '%".$query."%'")->fetchAll();

$player=$bdd->query(" SELECT *
            FROM player 
            WHERE post LIKE '%".$query."%' OR nom LIKE '%".$query."%' OR prenom  LIKE '%".$query."%' OR cat  LIKE '%".$query."%' OR jersey  LIKE '%".$query."%'")->fetchAll();
$res=array("act","flash","player","fixtures","trophy","his");
$tag=array("","Acualité","Flashinfo","Joueurs","Matchs","Trophés","Histoire & Légende");
 ?>
<div class="container main-slider" id="asm">
  <br>
<br>
<br>
<br>
<div class="headertitle">Résultat recherche : <?php echo $query;?><br></div>
<br>
<br>

<div class="row">
<?php if ( !$act && !$his && !$flash && !$fixtures && !$trophy && !$player )
echo '<div class="notfound">Aucun résultat trouvé.</div>';
else { ?>
<div id="tabs">
  <ul>
  <?php $cmp=1; foreach ($res as $key) {
    if (!empty($$key)) echo '<li><a href="#tabs-'.$cmp.'"';
    if (!empty($$key)&&($key=="player")) echo ' id="player"';
    if (!empty($$key)) echo '>'.$tag[$cmp].'</a></li>';
    $cmp++;
  }
    ?>
  </ul>
  <?php $cmp=1; foreach ($res as $key) {
    if (!empty($$key)) {
      echo '<div id="tabs-'.$cmp.'"><br><br><table>';
      switch ($key) {
        case 'act':
          foreach ($$key as $var) {
            echo '<tr><td colspan="2"><a href="#">'.$var['title'].'</a></td><td>'.html_entity_decode($var['article']).'</td></tr>';
          }
            
            break;

        case 'flash':
          foreach ($$key as $var) {
            echo '<tr><td colspan="2">'.$var['title'].'</td><td>'.html_entity_decode($var['text']).'</td></tr>';
          }
            
            break;

        case 'player':
          foreach ($$key as $var) {
            $date1 = $var['date'];
            $date2= date("Y-m-d");
            $diff = abs(strtotime($date2) - strtotime($date1));
            $years = floor($diff / (365*60*60*24));
          echo '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
<div class="flipeer-container">
<div class="flip-container flipeer">
  <div class="flipper">
    <div class="front" style="background: url(./img/players/'.$var['pid'].'.jpg) 0 0 no-repeat;">
    </div>
    <div class="back">
      <div class="back-title">'.$var['prenom'].' '.$var['nom'].'</div>
                <p>
              - ';
          $date1 = $var['date'];
          $date2= date("Y-m-d");
          $diff = abs(strtotime($date2) - strtotime($date1));
          $years = floor($diff / (365*60*60*24));
          echo $var['date'].' ( '.$years.'ans )';
          echo '<br>
              - Poste : '.$var['post'].'<br>
              - Taille : '.$var['height'].'CM<br>
              - Poids : '.$var['weight'].'kg<br>
              - Section : '.$var['cat'].'<br>
              
              </p>
    </div>
  </div>
  <div class="downinfo">
  <div class="jersey">'.$var['jersey'].'</div>
  <div class="player_name"><strong class="player"> '.$var['nom'].'</strong><span class="post">'.$var['post'].'</span></div>
  </div>
</div>
</div>

</div>';
          }
            
            break;

        case 'trophy':
          foreach ($$key as $var) {
            echo '<tr><td><img height="100" src="./img/trophies/'.$var['cat'].$var['trname'].$var['season'].'.png"></td><td>'.$var['trname'].'</td><td>'.$var['season'].'</td></tr>';
          }
            
            break;

        case 'fixtures':
          
            #beginFixtureSlider
echo '<div class="suivi">';

  $req3=$bdd->query('SELECT *, ABS(DATEDIFF(f.date,CURDATE())) as dte FROM fixtures as f, clubs as c WHERE (c.clid=f.clid AND (c.cname LIKE "%'.$query.'%" OR c.cat LIKE "%'.$query.'%" OR f.venue LIKE "%'.$query.'%" )) ORDER BY dte ASC')->fetchAll();

  
  echo '<table  style="border : 3px ;">';
  foreach($req3 as $data3){
      $score_mar="";
      $score_opp="";
    if($data3['venue']=="Stade Abdelaziz Chtioui") {
      $home=true;
      if ($data3['score']!=""){
      $score_mar=explode("-",$data3['score'])[0];
      $score_opp=explode("-",$data3['score'])[1];
      }
    }
    else{
      $home=false;
       if ($data3['score']!=""){
      $score_mar=explode("-",$data3['score'])[1];
      $score_opp=explode("-",$data3['score'])[0];
    }
    }
  echo '<tr>
    <ul class="result-slider"  style="text-align : center ;">
    
     <li><img src="img/TeamLogos/'; if($home) echo "ASM"; else echo $data3['cname'];
     echo '.png"  width="107px" class="img-responsive rotate180"></li>
     <li><h2>'; if($home) echo $score_mar; else echo $score_opp; 
     echo '</h2></li>
     <li><h2>-</h2></li>
     <li><h2>';
      if(!$home) echo $score_mar; else echo $score_opp;
      echo '</h2></li>
     <li><img src="img/TeamLogos/';
      if(!$home) echo "ASM"; else echo $data3['cname']; 
      echo '.png" width="107px"  class="img-responsive"></li>
    </ul>';
    echo '
      <div class="text-center center-block info-match">
      <h3>'.$data3['venue']." ";
      $date = date_create($data3['date']);
      echo date_format($date, 'd-m-Y').'</h3>
      <h3>';
       echo date_format($date, 'H:i').'</h3>
      </div></tr>'; }
    echo '
    <div class="slider-part2">';
    foreach($req3 as $data3){ 
       } 
      echo '</div>
  </div>'  ;





            #endFixtureSlider

            break;


       case 'his':
          foreach ($$key as $var) {
            echo '<tr><td><img height="150" src="./img/history/'.$var['hid'].'.jpg"></td><td>'.$var['title'].'</td><td>'.html_entity_decode($var['text']).'</td></tr>';
          }
            break;
        
      }
      echo '</table></div>';
    }
    $cmp++;
  }
    ?>
  
  
  
</div>
<?php 
}
?> 

</div>


</div>

<!-- ******************** FOOTER *********************** -->

<?php include('footer.php');?>
<!-- ******************** JS *********************** -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/custom-squad.js"></script>
  <script>
  $( function() {
   $( "#tabs" ).tabs();
   var query='<?php echo $query;?>';
   $('body').text().highlight(query);
   //unescape(query.replace(new RegExp('(\\b)(' + value + ')(\\b)','ig'), '$1<b>$2</b>$3'));
   
  } );
// $("td").each(function() {
//     var text = $(this).text();
//     var query='<?php //echo $query;?>';
//     var output = query.bold();
//     text = text.replace(query, output);
//     $(this).text(text);
// });


//   $(window).load(function() {
// var val = $("#tabs").val();
// $("#tabs").val(val.replace('Bale', 'ttttttttttttttt'));

//    var str =$('#tabs')[0].innerHTML;
//    str.replace(/bale/g, 'ffff');
//    console.log(str);
//    $('#tabs')[0].innerHTML=str;

//   });

 $('#tabs').on('tabsactivate',function(event, ui){
  // if(ui.newTab.context.id=="player") { ui.newPanel.context.css('border','0px');}
  // else {ui.newPanel.context.style.border=1; }
// console.log(event.which+"   "+ui);
 });


 // var query='<?php //echo $query;?>';
 // query=query.charAt(0).toUpperCase() + query.slice(1);
 // var queryMaj='<strong>'+query+'</strong>';
 // var queryJ=queryMaj.toString(); 
 //    $("body").html($("body").html().replace(query,queryJ));
  </script>

</body>
</html>