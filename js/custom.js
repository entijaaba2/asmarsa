// ========== Search bar =========== //


$(document).ready(function(){
    $("#search-input").hide();
    $("#search-glyph").click(function(){
        $("#search-input").fadeToggle(500);
        document.getElementById("search-input").focus();
    });
});

$(function() {
        
        $( "#search-input" ).autocomplete({
          source: "search.php",
          minLength: 1
        });
        });


// ========== NAV  =========== //
var w = $(window).width();
$('.main-list').hide();

$(document).ready(function() {
  
  $(window).scroll(function () {

    if ($(window).scrollTop() > 260) {
      $('.navbar').animate({
        opacity: 0},
        1, function() {
      $('.navbar').css({
        position: 'fixed',
        height: '20px',
        opacity:'1'
      });
      });
      $('.circle-container').hide();
      $('.sponsors-nav').hide();
      $('.main-list').show();
    }
    if ($(window).scrollTop() < 261) {
      $('.navbar').removeClass('navbar-fixed');
      if (w > 700) {$('.circle-container').show();
      $('.sponsors-nav').show();
      $('.main-list').hide();}
      
    }
  });
});

// ========== SORTING =========== //
// init Isotope
var $grid = $('.grid').isotope({
  // options
});
// filter items on button click
$('.filter-button-group').on( 'click', 'button', function() {
  var filterValue = $(this).attr('data-filter');
  $grid.isotope({ filter: filterValue });
});
//fixing Isotope Chrome issue
$(window).load(function() {
    $('#allB').click();
  });

// ========== DROPDOWN =========== //
var menuBox = document.getElementById('menu-box');
var menuBox2 = document.getElementById('menu-box2'); 
function toggleMenu() {
     
  if(menuBox.style.display == "inline-block") { // if is menuBox displayed, hide it
    menuBox.style.display = "none";
  }
  else { // if is menuBox hidden, display it
    menuBox.style.display = "inline-block";
    menuBox2.style.display = "none";
  }
}

function toggleMenu2() {
     
  if((menuBox2.style.display == "inline-block")) { // if is menuBox displayed, hide it
    menuBox2.style.display = "none";
  }
  else { // if is menuBox hidden, display it
    menuBox2.style.display = "inline-block";
    menuBox.style.display = "none";
  }
}


// ========== Pole =========== //
 function answerPole(id){
    var answer;
    if (document.getElementById("radio1").checked) answer="fa"; else if (document.getElementById("radio2").checked) answer="sa";
    $.ajax({
                        url: "answerPole.php",
                        method:"POST",
                        data: {'answer' : answer, 'id' : id},
                        success: function(data){  
                            
                document.getElementById("poleContent").innerHTML=data;
              
                        }
                    });
  }

// ========== NAVBAR =========== //

// $('body').click(function(event) {

//       if ($(event.target).closest('.navbar').length === 0) { 
//         $('#menu-box').css({
//           display: 'none'
//         });
//         $('#menu-box2').css({
//           display: 'none'
//         });
//       }

// });

$('#menu').hover(function() {
  $('#menu-box').css({
    display: 'inline-block'
  });
  $('#menu-box2').css({
    display: 'none'
  });
}, function() {});
$('#menu2').hover(function() {
  $('#menu-box2').css({
    display: 'inline-block'
  });
  $('#menu-box').css({
    display: 'none'
  });
}, function() {});

$('.main-list a').not('#menu').not('#menu2').hover(function() {
  $('#menu-box').css({
    display: 'none'
  });
  $('#menu-box2').css({
    display: 'none'
  });
}, function() {
  
});

$('.main-list a').not('#menu').not('#menu2').hover(function() {
  
}, function() {
  $('#menu-box').css({
    display: 'none'
  });
  $('#menu-box2').css({
    display: 'none'
  });
});

$('#bs-example-navbar-collapse-1').hover(function() {
  
}, function() {
  $('#menu-box').css({
    display: 'none'
  });
  $('#menu-box2').css({
    display: 'none'
  });
});
