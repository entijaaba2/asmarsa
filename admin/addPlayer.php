<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Ajouter un joueur - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<style>
td{
	padding-top: 15px;
}
</style>
</head> 
   
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$page="player";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="switches">
		<div class=" col-lg-6 col-lg-offset-3" >
		<h1 style="text-align: center; margin: 0 auto;">Ajouter un joueur</h1><br>
		<form class="navbar-form navbar-left" role="search" action="addPlayer2.php" enctype="multipart/form-data" method="post" >
		<div class="form-group" style="padding-left: 180px; ">
					 <select onchange="jerseyCheck();" id="category" class="form-control1" style="width : 300px;" name="cat">
							  
						<?php
								$req=$bdd->query('SELECT * FROM category WHERE visibility=1');
								while($data=$req->fetch()){ 
						?>
							  
							  <option value="<?php echo $data['cat'];?>"><?php echo $data['cat'];?></option>
						<?php } ?>
					 </select> 
				  </div>

      
	  <div style="padding-left: 150px;">
		  <table>
			<tr>
				<td align="right"> Nom : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="text" name="nom" required class="form-control" placeholder="Nom">
				  </div>
				</td>
			</tr>
			
			<tr>
				<td align="right"> Prénom : &nbsp&nbsp </td>
				<td>
				  <div class="form-group">
					<input type="text" name="prenom" required class="form-control" placeholder="Prénom">
				  </div>
				</td>
			</tr>
		  
			<tr>
				<td align="right"> Date de naissance : &nbsp&nbsp </td>
				<td>
				  <div class="form-group">
					<input type="date" name="date" required class="form-control" placeholder="aaaa-mm-jj">
				  </div>
				</td>
			</tr>
			
			
			
			
			<tr>
				<td align="right"> Taille (cm) : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="number" step="0.1" name="height" required class="form-control" placeholder="Taille">
				  </div>
				</td>
			</tr>
			
			<tr>
				<td align="right"> Poids (kg) : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="number" step="0.1" name="weight" required class="form-control" placeholder="Poids">
				  </div>
				</td>
			</tr>

			<tr>
				<td align="right"> Numéro maillot : &nbsp&nbsp </td>
				<td>
				  <div id="validateJersey" class="form-group">
					<input type="number" id="jerseynumber" onchange="jerseyCheck();" name="jersey" required class="form-control" placeholder="Numéro maillot">
				  </div>
				</td>
			</tr>

		
			
			<tr>
				<td align="right"> Poste : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					 <select  class="form-control1"  name="post">
							  <option value="Gardien">Gardien</option>
							  <option value="Défenseur central">Défenseur central</option>
							  <option value="Arrière droit">Arrière droit</option>
							  <option value="Arrière gauche">Arrière gauche</option>
							  <option value="Milieu défensif">Milieu défensif</option>
							  <option value="Pivot">Pivot</option>
							  <option value="Milieu offensif">Milieu offensif</option>
							  <option value="Aillier">Aillier</option>
							  <option value="Attaquant">Attaquant</option>
					 </select> 
				  </div>
				</td>
			</tr>

			<tr>
				<td align="right"> Image : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input required type="file" name="image" >
				  </div>
				</td>
			</tr>
		</table>
		 <br>
		<div id="felsa">
		 <button  class="btn btn-success" style="margin-left: 150px;" type="submit">Ajouter</button>
		</div>
		</div>
		 </form>
		  

		
		
		</div>
		</div>
		
		
		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
   
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
<!-- Modal content-->
   <div <?php if (!empty($_GET)) {if ($_GET['r']=="success") echo 'class="alert alert-success"'; else echo 'class="alert alert-danger"';}?>>
    <h1 style="text-align :center ;"><?php if (!empty($_GET)) {if ($_GET['r']=="success") echo 'Opération réussie'; else { if ($_GET['r']=="failure") echo 'Echec de l\'opération'; else echo '404'; } } ?></h1> 
<br>
<br>
		<h5 style="text-align :center ;"><?php if (!empty($_GET)) { if ($_GET['r']=="success") echo 'Joueur ajouté avec succès!'; else echo 'Une erreur est survenue';}?></h5>
  </div>
   </div>
</div>
 <!-- JS Files -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
function modalShow(){
	if(window.location.search){
	$('#myModal').modal('show');
	}
}
modalShow();

function jerseyCheck(){
var jersey= document.getElementById('jerseynumber').value;
var category= document.getElementById('category').value;
$.ajax({
                        url: "jerseyCheck.php",
                        method:"POST",
                        data: {'jersey': jersey, 'category' : category},
                        success: function(data){  
                            switch(data){
							
							case "0" :
								document.getElementById('validateJersey').innerHTML= '<div id="validateJersey" class="form-group has-success">' +
																					 '<input type="number" value="'+jersey+'" id="jerseynumber" onchange="jerseyCheck();" name="jersey" required class="form-control" placeholder="Numéro maillot">' +
																					 '</div>';
								document.getElementById('felsa').innerHTML='<button  class="btn btn-success" style="margin-left: 150px;" type="submit">Ajouter</button>'
								
								break;
								
								
							default :
								document.getElementById('validateJersey').innerHTML= '<div id="validateJersey" class="form-group has-error">' +
																					 '<input type="number" value="'+jersey+'" id="jerseynumber" onchange="jerseyCheck();" name="jersey" required class="form-control" placeholder="Numéro maillot">' +
																					 '</div>';
								document.getElementById('felsa').innerHTML='<button class="btn btn-success disabled" style="margin-left: 150px;" type="submit">Ajouter</button>'
								break;

							}
								
							
                        }
                    });
}
</script>


<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>

</body>
</html>