<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Ajouter une actualité - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">

<style>
td{
	padding-top: 15px;
}

.tscali{
    -ms-transform: scale(2, 3); /* IE 9 */
    -webkit-transform: scale(2, 3); /* Safari */
    transform: scale(2, 3);
}
</style>
</head> 
   <?php
include_once("connect_to_base.php");

if((isset($_POST))&&(!empty($_POST))){
	$time=date("Y-m-d H:i:s");
	$bdd->query('INSERT into actualite(cat,title,article,date) VALUES ("'.$_POST['cat'].'", "'.$_POST['title'].'", "'. htmlentities($_POST['article']).'","'.$time.'")');
	$url = $_POST['image'];
	$id=$bdd->query('SELECT acid FROM actualite ORDER BY acid DESC LIMIT 1')->fetch()[0];
	$img = "../img/actualite/tmp/".$id.".jpg";
	file_put_contents($img, file_get_contents($url));
	include('resize.php');
	resizeTaswira($img,1400,650,"../img/actualite/".$id);
	foreach (glob("../img/actualite/tmp/*") as $filename) {
    unlink($filename);
		}
	header('Location: addActu.php');
	die();
} 

?>
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$page="act";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="switches">
		<div class=" col-lg-12" >
		<h1>Ajouter une actualité :</h1><br>
		<div >
		<form class="navbar-form navbar-left" role="search" action="addActu.php" enctype="multipart/form-data" method="post" >
		 <div class="col-lg-10">
		<div class="form-group" style="margin-left : 60px;">
					 <select class="form-control1" style="width : 300px;" name="cat">
							  
						<?php
								$req=$bdd->query('SELECT * FROM category WHERE visibility=1');
								while($data=$req->fetch()){ 
						?>
							  
							  <option value="<?php echo $data['cat'];?>"><?php echo $data['cat'];?></option>
						<?php } ?>
					 </select> 
				  </div>

      
	 
		  <table>
			<tr>
				<td align="left"> Titre : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="text" pattern=".{1,100}" style="width : 300px;" required title="100 caractères au maximum" name="title" required class="form-control" placeholder="Article">
				  </div>
				</td>
			</tr>
			
			<tr>
				<td align="left"> Article&nbsp: &nbsp&nbsp </td>
				<td>
				  <div class="form-group">
					<textarea name="article" rows="10" cols="70" placeholder="L'article..."></textarea>
				  </div>
				</td>
			</tr>
		  
		

		</table>
		 <br>

		 
		<div id="demos"></div></div>
		<div class="col-lg-2" id="felsa">
		 <button id="bou" class="btn btn-success" disabled="" style="margin-left: 150px;" type="submit">Ajouter</button>
		</div>
		</div>
		 </form>
		 </div>
		 <div class="col-lg-12">
		  Image : &nbsp&nbsp
<form id="uploadimage" action="" method="post" enctype="multipart/form-data">
					<input required type="file" id="taswira" name="image" ></form>
		
		<div id="vanilla-demo"></div>
		<button style="margin-left : 300px;" id="pelsa" class="btn btn-warning" onclick="croppi();">Crop</button>
		</div>
		</div>
		
		
		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			
        <!--footer section end-->

      <!-- main content end-->
   </section>
   		
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
<!-- Modal content-->
   <div <?php if (!empty($_GET)) {if ($_GET['r']=="success") echo 'class="alert alert-success"'; else echo 'class="alert alert-danger"';}?>>
    <h1 style="text-align :center ;"><?php if (!empty($_GET)) {if ($_GET['r']=="success") echo 'Opération réussie'; else { if ($_GET['r']=="failure") echo 'Echec de l\'opération'; else echo '404'; } } ?></h1> 
<br>
<br>
		<h5 style="text-align :center ;"><?php if (!empty($_GET)) { if ($_GET['r']=="success") echo 'Actualité ajoutée avec succès!'; else echo 'Une erreur est survenue';}?></h5>
  </div>
   </div>
</div>
 <!-- JS Files -->
<script src="js/jquery.js"></script>
<script src="js/jquery.form.js"></script>
<link rel="stylesheet" href="css/croppie.css" />
<script src="js/croppie.js"></script>
<script>
$(document).ready(function(){
	$('#pelsa').hide();
    $('[data-toggle="tooltip"]').tooltip();
});
function modalShow(){
	if(window.location.search){
	$('#myModal').modal('show');
	}
}
modalShow();

var vanilla = new Croppie($('#vanilla-demo')[0], {
	boundary: { width: 970, height: 650 },
    viewport: { width: 950, height: 530 },
    enableOrientation: true
});
$( "#taswira" ).change(function() {
	$('#vanilla-demo')[0].innerHTML="";
	vanilla = new Croppie($('#vanilla-demo')[0], {
	boundary: { width: 970, height: 650 },
    viewport: { width: 950, height: 530 },
    enableOrientation: true
});

$('#uploadimage').ajaxSubmit({
url: "testingupload.php", // Url to which the request is send
type: "POST",             // Type of request to be send, called as method
data: $('#uploadimage').serialize(),// Data sent to server, a set of key/value pairs (i.e. form fields and values)
success: function(data)   // A function to be called if request succeeds
{
$('#pelsa').show();
vanilla.bind({
    url: data
});
}
});
});


function croppi(){
	vanilla.result('canvas').then(function(base64Image) {
    document.getElementById("demos").innerHTML='<input type="hidden" name="image" value="'+base64Image+'">';
    $('#bou').removeAttr("disabled");
});
}

</script>

<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>tinymce.init({ selector:'textarea' });</script>
</body>
</html>