// ========== Search bar =========== //


$(document).ready(function(){
    $("#search-input").hide();
    $("#search-glyph").click(function(){
        $("#search-input").fadeToggle(500);
        document.getElementById("search-input").focus();
    });
});

// ========== Search bar =========== //

$('.flip-container').on("touchstart", function() {
    // this.classList.toggle('hover');
});

// ========== DROPDOWN =========== //
var menuBox = document.getElementById('menu-box');
var menuBox2 = document.getElementById('menu-box2'); 
function toggleMenu() {
     
  if(menuBox.style.display == "inline-block") { // if is menuBox displayed, hide it
    menuBox.style.display = "none";
  }
  else { // if is menuBox hidden, display it
    menuBox.style.display = "inline-block";
    menuBox2.style.display = "none";
  }
}

function toggleMenu2() {
     
  if((menuBox2.style.display == "inline-block")) { // if is menuBox displayed, hide it
    menuBox2.style.display = "none";
  }
  else { // if is menuBox hidden, display it
    menuBox2.style.display = "inline-block";
    menuBox.style.display = "none";
  }
}

$(function() {
        
        $( "#search-input" ).autocomplete({
          source: "search.php",
          minLength: 1
        });
        });