<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Les utilisateurs - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->

<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">

</head> 
   
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$page="users";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
			<h2>Liste des utilisateurs :</h2><br>
			<table  id="example" class="table table-hover">
										<thead>
										  <tr>
											<th>Email</th>
											
										  </tr>
										</thead>
										
										<tbody>
											<?php $req=$bdd->query('SELECT * FROM user ORDER BY uid DESC');
													while($user=$req->fetch()){ ?>
										  <tr>
											<td><?php echo $user['email'];?></td>
										  </tr>
										<?php } ?>
										</tbody>
									  </table>
			
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<script>	
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>	  
</body>
</html>