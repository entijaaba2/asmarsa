<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Ajouter un match- Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">

<style>
td{
	padding-top: 15px;
}
</style>
</head> 
   <?php
include_once("connect_to_base.php");
if((isset($_POST))&&(!empty($_POST))){

	if((($_POST['newclub']=="")||(!$_FILES['image']['tmp_name'])) && ($_POST['oldclub']=="#")) { header('Location: addMatch.php?r=failure'); die();}
	else {
		$clid="";
		if ($_POST['oldclub']=="#") {
			$cname=$_POST['newclub'];
			$bdd->query('INSERT into clubs(cat,cname) VALUES ("'.$_POST['cat'].'", "'.$cname.'")');
			move_uploaded_file($_FILES['image']['tmp_name'], "../img/TeamLogos/".$cname.".png");
			$clid=$bdd->query('SELECT clid FROM clubs ORDER BY clid DESC LIMIT 1')->fetch()[0];
		}
		else {
			$clid=$_POST['oldclub'];
		}
		$bdd->query('INSERT into fixtures(clid,score,date,venue) VALUES ("'.$clid.'", "'.$_POST['score'].'", "'.$_POST['date'].'", "'.$_POST['venue'].'")');
		header('Location: addMatch.php?r=success');
		die();

	}
	
	
} 

?>
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$page="match";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="switches">
		<div class=" col-lg-12" >
		<h1>Ajouter un match :</h1><br>
		<form class="navbar-form navbar-left" role="search" action="addMatch.php" enctype="multipart/form-data" method="post" >
		<div class="form-group" style="margin-left : 138px;">
					 <select class="form-control1" id="category" required onchange="getClubs();" style="width : 300px;" name="cat">
							  
						<?php
								$req=$bdd->query('SELECT * FROM category  WHERE visibility=1');
								$first=false;
								while($data=$req->fetch()){ 
									if ($first==false) { $first=true; $firstcat=$data['cat'];}
						?>
							  
							  <option value="<?php echo $data['cat'];?>"><?php echo $data['cat'];?></option>
						<?php } ?>
					 </select> 
				  </div>

      
	  <div>
		  <table>
			<tr>
				<td align="left"> Equipe : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					 <select class="form-control1" id="equipet" onchange="logoDisplay();" style="width : 150px;" name="oldclub">
							  <option value="#">Equipe</option>
						<?php
								$req2=$bdd->query('SELECT * FROM clubs WHERE cat="'.$firstcat.'" ');
								while($data2=$req2->fetch()){ 
						?>
							  
							  <option value="<?php echo $data2['clid'];?>"><?php echo $data2['cname'];?></option>
						<?php } ?>
					 </select> 
					<div id="logo"></div>
				  </div>
					<br>
					<br>
				  <div class="form-group">
				  <h4 style="color : #FF0000;">Si l'équipe n'existe pas dans la liste ajoutez la</h4>
					<input type="text" style="width : 300px;" name="newclub" class="form-control" placeholder="Ex : RM">
					<br><br><h5 style="color : #FF0000;">Ajoutez une photo du format png avec un arrière plan transparent</h5>
					<input class="image" type="file" name="image" >
				  	
				  </div>


				</td>
			</tr>

			<tr>
				<td align="left"> Score : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="text" style="width : 300px;" name="score" class="form-control" placeholder="Laissez vide pour les matchs à venir">
				  </div>
				</td>
			</tr>

			<tr>
				<td align="left"> La date et heure : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="datetime" style="width : 300px;" name="date" required class="form-control" placeholder="Ex : 2016-09-13 15:00">
				  </div>
				</td>
			</tr>



			<tr>
				<td align="left"> Le stade : &nbsp&nbsp</td>
				<td>
				  <div class="form-group"><input type="text" style="width : 300px;" name="venue" required class="form-control" placeholder="Ex : Abdel Aziz Chtioui"></div>
					
				</td>
			</tr>
			
			
		  
			

		</table>
		 <br>
		<div id="felsa">
		 <button  class="btn btn-success" style="margin-left: 150px;" type="submit">Ajouter</button>
		</div>
		</div>
		 </form>
		  

		
		
		</div>
		</div>
		
		
		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
   
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
<!-- Modal content-->
   <div <?php if (!empty($_GET)) {if ($_GET['r']=="success") echo 'class="alert alert-success"'; else echo 'class="alert alert-danger"';}?>>
    <h1 style="text-align :center ;"><?php if (!empty($_GET)) {if ($_GET['r']=="success") echo 'Opération réussie'; else { if ($_GET['r']=="failure") echo 'Echec de l\'opération'; else echo '404'; } } ?></h1> 
<br>
<br>
		<h5 style="text-align :center ;"><?php if (!empty($_GET)) { if ($_GET['r']=="success") echo 'Actualité ajoutée avec succès!'; else echo 'Une erreur est survenue';}?></h5>
  </div>
   </div>
</div>
 <!-- JS Files -->
<script src="js/jquery.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="js/match.js"></script>


<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>

</body>
</html>