<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Modifier une actualité - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<style>
td{
	padding-top: 15px;
}
</style>
</head> 
   <?php
include_once("connect_to_base.php");
$actu="";
if((isset($_POST))&&(!empty($_POST))){
	$bdd->query('UPDATE staff SET nom="'.$_POST['nom'].'", prenom="'.$_POST['prenom'].'", post="'.$_POST['post'].'"   WHERE stid="'.$_GET['id'].'"');
	if($_FILES['image']['tmp_name']!='') 
		move_uploaded_file($_FILES['image']['tmp_name'], "../img/staff/".$_GET['id'].".jpg");
	header('Location: allStaff.php?r=success');
	die();
} 
if(isset($_GET)){

				if($staff=$bdd->query('SELECT * FROM staff WHERE stid="'.$_GET['id'].'"')->fetch());
				else {
					header('Location: allStaff.php');
					die();
				}
				
				} else {
					header('Location: allStaff.php');
					die();
				}


?>
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$page="act";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="switches">
		<div class=" col-lg-12" >
		<div class=" col-lg-10" >
		<h1>Modifier le profil d'un membre du staff :</h1><br>
		</div>
		<div class="col-lg-2">
		<a href="allStaff.php"><button class="btn btn-success">Voir tout le staff</button></a>
		</div>
		<form class="navbar-form navbar-left" role="search" action="editStaff.php?id=<?php echo $_GET['id'];?>" enctype="multipart/form-data" method="post" >
		<div class="col-lg-5">
	<img src="../img/staff/<?php echo $staff['stid']; ?>.jpg" width="200" alt="">
	<br>
			<tr>
				<td align="right"> Photo : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input class="image" type="file" name="image" >
				  </div>
				</td>
			</tr>
	</div>

		<div class="col-lg-7" style="margin-bottom: 20vh;">
		  <table>
			<tr>
				<td align="right"> Nom : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="text" style="width : 300px;" name="nom" class="form-control" value="<?php echo $staff['nom'];?>">
				  </div>
				</td>
			</tr>

			<tr>
				<td align="right"> Prénom : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="text" style="width : 300px;"  name="prenom" class="form-control" value="<?php echo $staff['prenom'];?>">
				  </div>
				</td>
			</tr>

			<tr>
				<td align="right"> Poste : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="text" style="width : 300px;" name="post" class="form-control" value="<?php echo $staff['post'];?>">
				  </div>
				</td>
			</tr>
	
	</table>

	</div>


		 <br>
		<div id="felsa">
		 <button  class="btn btn-success" style="margin-left: 150px;" type="submit">Modifier</button>
		</div>
		</div>
		 </form>
		  

		
		
		</div>
		</div>
		
		
		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  
 <!-- JS Files -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});

</script>

<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>

</body>
</html>