<?php 
// upload.php
// 'images' refers to your file input name attribute
if (empty($_FILES['input44'])) {
    echo json_encode(['error'=>'No files found for upload.']); 
    // or you can throw an exception 
    return; // terminate
}


// get the files posted
$images = $_FILES['input44'];
// a flag to see if everything is ok
$success = null;

// file paths to store
$paths= [];
include('resize.php');
// get file names
$filenames = $images['name'];
$tim=date("U");
if (strpos($_POST['folder'],'../img/gallery/')) {
    $folder=$_POST['folder'];
}
else {
    $folder="../img/gallery/" . $_POST['section'] ."=" . $_POST['folder']."=".$tim ;
    mkdir($folder);
}
$f = fopen($folder."/order.txt", "w");
$count = count(glob($folder."/*"));
if($count+sizeof($images)<=13){
    // loop and process files
for($i=0; $i < count($filenames); $i++){
    $ext = explode('.', basename($filenames[$i]));
    $time=rand(1,10000);
    $type=array_pop($ext);
    $tmp = $folder."/tmp.".$type ;
    $target = $folder. '/' .$_POST['section']."=". $time;

    if(move_uploaded_file($images['tmp_name'][$i], $tmp)) {
        $success = true;
        resizeTaswira($tmp,475,266,$target);
        unlink($tmp);
        fwrite($f, explode("/",$target)[4].".".$type);
        fwrite($f, PHP_EOL);
        
        
    } else {
        $success = false;
        break;
    }
}
}
else echo 'L\'opération a échoué ! Vous n\'avez pas le droit d\'ajouter plus que 13 photos par albums!';
fclose($f);

// check and process based on successful status 
if ($success === true) {
    // call the function to save all data to database
    // code for the following function `save_data` is not 
    // mentioned in this example
    // store a successful response (default at least an empty array). You
    // could return any additional response info you need to the plugin for
    // advanced implementations.
    $output = [];
    // for example you can get the list of files uploaded this way
    // $output = ['uploaded' => $paths];
} elseif ($success === false) {
    $output = ['error'=>'Error while uploading images. Contact the system administrator'];
    
} else {
    $output = ['error'=>'No files were processed.'];
}

// return a json encoded response for plugin to process successfully
echo json_encode($output);
?>