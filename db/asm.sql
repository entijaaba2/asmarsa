-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 28, 2017 at 04:56 PM
-- Server version: 5.5.54-0+deb8u1
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `asm`
--

-- --------------------------------------------------------

--
-- Table structure for table `actualite`
--

CREATE TABLE IF NOT EXISTS `actualite` (
`acid` int(11) NOT NULL,
  `cat` varchar(50) NOT NULL,
  `title` text NOT NULL,
  `article` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `actualite`
--

INSERT INTO `actualite` (`acid`, `cat`, `title`, `article`, `date`) VALUES
(59, 'volleyball', 'ASM vs EST', '&lt;p&gt;Nullam ac ipsum id tellus vulputate tristique. Duis a condimentum ante. Etiam scelerisque orci libero, id volutpat ex interdum ac. Suspendisse vitae imperdiet lorem. Nulla in euismod dolor. Aliquam gravida faucibus pharetra. Suspendisse vehicula malesuada dolor a aliquam. Duis gravida arcu vitae turpis hendrerit placerat. Suspendisse dapibus, urna sed dictum pulvinar, metus mauris fringilla magna, vel aliquet ex ex in felis. Vivamus mi enim, condimentum nec maximus vitae, efficitur et diam. Praesent pellentesque viverra sollicitudin. Morbi velit turpis, imperdiet at justo vitae, dignissim consequat purus. Proin imperdiet bibendum arcu, vitae pellentesque mauris. Pellentesque pellentesque nulla in urna dapibus vulputate.&lt;/p&gt;', '2016-08-30 14:52:39'),
(60, 'football', 'Accord avec nouveau sponsor', '&lt;p&gt;Nullam ac ipsum id tellus vulputate tristique. Duis a condimentum ante. Etiam scelerisque orci libero, id volutpat ex interdum ac. Suspendisse vitae imperdiet lorem. Nulla in euismod dolor. Aliquam gravida faucibus pharetra. Suspendisse vehicula malesuada dolor a aliquam. Duis gravida arcu vitae turpis hendrerit placerat. Suspendisse dapibus, urna sed dictum pulvinar, metus mauris fringilla magna, vel aliquet ex ex in felis. Vivamus mi enim, condimentum nec maximus vitae, efficitur et diam. Praesent pellentesque viverra sollicitudin. Morbi velit turpis, imperdiet at justo vitae, dignissim consequat purus. Proin imperdiet bibendum arcu, vitae pellentesque mauris. Pellentesque pellentesque nulla in urna dapibus vulputate.&lt;/p&gt;', '2016-08-30 14:53:11'),
(61, 'basketball', 'Travaux au stade Abdelaziz Chtioui', '&lt;p&gt;Nullam ac ipsum id tellus vulputate tristique. Duis a condimentum ante. Etiam scelerisque orci libero, id volutpat ex interdum ac. Suspendisse vitae imperdiet lorem. Nulla in euismod dolor. Aliquam gravida faucibus pharetra. Suspendisse vehicula malesuada dolor a aliquam. Duis gravida arcu vitae turpis hendrerit placerat. Suspendisse dapibus, urna sed dictum pulvinar, metus mauris fringilla magna, vel aliquet ex ex in felis. Vivamus mi enim, condimentum nec maximus vitae, efficitur et diam. Praesent pellentesque viverra sollicitudin. Morbi velit turpis, imperdiet at justo vitae, dignissim consequat purus. Proin imperdiet bibendum arcu, vitae pellentesque mauris. Pellentesque pellentesque nulla in urna dapibus vulputate.&lt;/p&gt;', '2016-08-30 14:54:19'),
(62, 'volleyball', 'Recrutement de Bouselmi', '&lt;p&gt;Nullam ac ipsum id tellus vulputate tristique. Duis a condimentum ante. Etiam scelerisque orci libero, id volutpat ex interdum ac. Suspendisse vitae imperdiet lorem. Nulla in euismod dolor. Aliquam gravida faucibus pharetra. Suspendisse vehicula malesuada dolor a aliquam. Duis gravida arcu vitae turpis hendrerit placerat. Suspendisse dapibus, urna sed dictum pulvinar, metus mauris fringilla magna, vel aliquet ex ex in felis. Vivamus mi enim, condimentum nec maximus vitae, efficitur et diam. Praesent pellentesque viverra sollicitudin. Morbi velit turpis, imperdiet at justo vitae, dignissim consequat purus. Proin imperdiet bibendum arcu, vitae pellentesque mauris. Pellentesque pellentesque nulla in urna dapibus vulputate.&lt;/p&gt;', '2016-08-30 14:56:09'),
(63, 'football', 'Supportaires satisfaits', '&lt;p&gt;Aenean eu arcu velit. Nam mattis feugiat diam sit amet condimentum. Pellentesque non diam mollis, egestas purus a, vulputate risus. Nulla velit nunc, feugiat sit amet nulla quis, scelerisque accumsan mi. Quisque gravida dapibus aliquet. Vestibulum viverra est vitae nisi scelerisque tincidunt. Curabitur aliquam scelerisque justo in porta. Cras vel nunc neque. Phasellus convallis lacus et ipsum vestibulum laoreet. Curabitur interdum ut metus id mollis. Nam et rhoncus sapien. Quisque eget ligula nec dui sagittis sodales eu in enim. Suspendisse velit eros, convallis nec maximus nec, sollicitudin feugiat magna.&lt;/p&gt;', '2016-08-30 15:21:51'),
(64, 'football', 'Match amical', '&lt;p&gt;Aenean eu arcu velit. Nam mattis feugiat diam sit amet condimentum. Pellentesque non diam mollis, egestas purus a, vulputate risus. Nulla velit nunc, feugiat sit amet nulla quis, scelerisque accumsan mi. Quisque gravida dapibus aliquet. Vestibulum viverra est vitae nisi scelerisque tincidunt. Curabitur aliquam scelerisque justo in porta. Cras vel nunc neque. Phasellus convallis lacus et ipsum vestibulum laoreet. Curabitur interdum ut metus id mollis. Nam et rhoncus sapien. Quisque eget ligula nec dui sagittis sodales eu in enim. Suspendisse velit eros, convallis nec maximus nec, sollicitudin feugiat magna.&lt;/p&gt;', '2016-08-30 15:22:35'),
(65, 'football', 'ASM vs CSHL', '&lt;p&gt;Aenean eu arcu velit. Nam mattis feugiat diam sit amet condimentum. Pellentesque non diam mollis, egestas purus a, vulputate risus. Nulla velit nunc, feugiat sit amet nulla quis, scelerisque accumsan mi. Quisque gravida dapibus aliquet. Vestibulum viverra est vitae nisi scelerisque tincidunt. Curabitur aliquam scelerisque justo in porta. Cras vel nunc neque. Phasellus convallis lacus et ipsum vestibulum laoreet. Curabitur interdum ut metus id mollis. Nam et rhoncus sapien. Quisque eget ligula nec dui sagittis sodales eu in enim. Suspendisse velit eros, convallis nec maximus nec, sollicitudin feugiat magna.&lt;/p&gt;', '2016-08-30 15:25:12'),
(66, 'basketball', 'rbttytry', '&lt;p&gt;erqyrthbrtbt&lt;/p&gt;', '2016-09-05 11:15:43'),
(74, 'football', 'Gnawiya', '&lt;p&gt;&lt;code&gt;&lt;span class=&quot;html&quot;&gt;if you use Apache as a webserver, you could do the following:&lt;br /&gt;&lt;br /&gt;You could set up a ''img'' directory in your webspace.&lt;br /&gt;In that directory there will be two files: a .htaccess file and a img.php file&lt;br /&gt;the .htaccess file contains the following code:&lt;br /&gt;ErrorDocument 404 /img/img.php&lt;br /&gt;&lt;br /&gt;the img.php file looks something like this i&lt;/span&gt;&lt;/code&gt;&lt;code&gt;&lt;span class=&quot;html&quot;&gt;n that directory there will be two files: a .htaccess file and a img.php file&lt;br /&gt;the .htaccess file contains the following code:&lt;br /&gt;ErrorDocument 404 /img/img.php&lt;/span&gt;&lt;/code&gt;&lt;/p&gt;', '2016-09-08 16:05:47'),
(75, 'basketball', 'kljhkl', '&lt;p&gt;ft-u&lt;/p&gt;', '2017-01-05 14:05:07');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`aid` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` text NOT NULL,
  `salt` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`aid`, `email`, `password`, `salt`) VALUES
(1, 'administrateur@gdice.com', 'eedbd850d7ce07a4ce7de9f8201c6e5336c42e0f382d93e6a7215728dadb3f8d', 'da432a16412de9e');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `cat` varchar(50) NOT NULL,
  `visibility` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`cat`, `visibility`) VALUES
('ATL', 0),
('basketball', 1),
('football', 1),
('handball', 1),
('Petanque', 0),
('volleyball', 1);

-- --------------------------------------------------------

--
-- Table structure for table `clubs`
--

CREATE TABLE IF NOT EXISTS `clubs` (
`clid` int(11) NOT NULL,
  `cname` varchar(50) NOT NULL,
  `cat` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clubs`
--

INSERT INTO `clubs` (`clid`, `cname`, `cat`) VALUES
(1, 'EST', 'football'),
(2, 'ESZ', 'basketball'),
(3, 'CSHL', 'football'),
(4, 'CAB', 'football'),
(5, 'JSK', 'football'),
(6, 'OB', 'football'),
(7, 'CA', 'volleyball'),
(8, 'ESS', 'football'),
(9, 'SG', 'football');

-- --------------------------------------------------------

--
-- Table structure for table `fixtures`
--

CREATE TABLE IF NOT EXISTS `fixtures` (
`fid` int(11) NOT NULL,
  `clid` int(50) NOT NULL,
  `score` varchar(10) NOT NULL,
  `date` datetime NOT NULL,
  `venue` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fixtures`
--

INSERT INTO `fixtures` (`fid`, `clid`, `score`, `date`, `venue`) VALUES
(1, 1, '2-1', '2016-08-15 17:00:00', 'Stade Abdelaziz Chtioui'),
(2, 3, '3-1', '2016-08-11 17:00:00', 'Stade Abdelaziz Chtioui'),
(4, 7, '78-34', '2016-08-27 19:00:00', 'Stade Abdelaziz Chtioui'),
(5, 8, '', '2016-09-01 18:00:00', 'Sousse'),
(6, 4, '4-0', '2016-08-29 16:00:00', 'Stade Abdelaziz Chtioui');

-- --------------------------------------------------------

--
-- Table structure for table `flashinfo`
--

CREATE TABLE IF NOT EXISTS `flashinfo` (
`flid` int(11) NOT NULL,
  `cat` varchar(50) NOT NULL,
  `title` text NOT NULL,
  `text` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flashinfo`
--

INSERT INTO `flashinfo` (`flid`, `cat`, `title`, `text`, `date`) VALUES
(4, 'handball', 'Maola suspendu', '&lt;p&gt;Dct that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ''Content here, content here'', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for ''lorem ipsum'' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).&lt;/p&gt;', '2016-08-30 08:09:52'),
(5, 'football', 'Khaled Ben Sassi nouveau coach ', '&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n&lt;div id=&quot;js_u&quot; class=&quot;_5pbx userContent&quot; style=&quot;font-size: 14px; font-weight: normal; line-height: 1.38; overflow: hidden; color: #1d2129; font-family: Helvetica, Arial, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: #ffffff;&quot; data-ft=&quot;{&amp;quot;tn&amp;quot;:&amp;quot;K&amp;quot;}&quot;&gt;\r\n&lt;p style=&quot;margin: 0px; display: inline;&quot;&gt;&lt;a class=&quot;_58cn&quot; style=&quot;color: #365899; cursor: pointer; text-decoration: none;&quot; href=&quot;https://www.facebook.com/hashtag/asm_officiel?source=feed_text&amp;amp;story_id=1033249730119381&quot; data-ft=&quot;{&amp;quot;tn&amp;quot;:&amp;quot;*N&amp;quot;,&amp;quot;type&amp;quot;:104}&quot;&gt;&lt;span class=&quot;_5afx&quot; style=&quot;direction: ltr; unicode-bidi: isolate;&quot;&gt;&lt;span class=&quot;_58cl _5afz&quot; style=&quot;unicode-bidi: isolate; color: #4267b2;&quot;&gt;#&lt;/span&gt;&lt;span class=&quot;_58cm&quot;&gt;ASM_OFFICIEL&lt;/span&gt;&lt;/span&gt;&lt;/a&gt;&amp;nbsp;: Officiellement Khaled ben Sassi nouvel Entra&amp;icirc;neur&lt;/p&gt;\r\n&lt;/div&gt;', '2016-11-08 13:39:20'),
(6, 'football', 'Cavani se sent bien a dar el Marsa ', '&lt;p&gt;L''accord est officlias&amp;eacute; , l''ASM recrute l''ancien attaquant du PSG.&amp;nbsp;&lt;/p&gt;', '2017-01-03 15:30:40'),
(7, 'football', 'Foulen commence son rÃ©gime ', '&lt;p&gt;xcxccxcxcxcxcxc&lt;/p&gt;', '2017-01-03 17:35:52'),
(8, 'volleyball', 'Victoire des vÃ©tÃ©rans', '&lt;p&gt;Ce dimanche l''&amp;eacute;quipe v&amp;eacute;t&amp;eacute;ran de l''Avenir a remport&amp;eacute; son match qui l''opposait &amp;agrave; l''AS Hammam Chatt sur le score de 2sets &amp;agrave; 0. &amp;nbsp;&lt;/p&gt;', '2017-01-04 11:34:07'),
(9, 'volleyball', 'Victoire contre Bou Selem', '&lt;p&gt;Notre &amp;eacute;quipe s&amp;eacute;nior a remport&amp;eacute; son 1er match des Play Out contre Bou Selem sur le score de 3 sets &amp;agrave; 1. Apr&amp;egrave;s le gain des 2 premiers sets les joueurs de Basdouri ont conc&amp;eacute;d&amp;eacute; le 3&amp;egrave;me set avant de se reprendre lors du dernier avec un score de 25-13.&lt;/p&gt;\r\n&lt;p&gt;Score: 28-26 / 25-18 / 25-27 / 25-13&lt;/p&gt;', '2017-01-04 11:35:13');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE IF NOT EXISTS `history` (
`hid` int(11) NOT NULL,
  `cat` varchar(50) NOT NULL,
  `title` text NOT NULL,
  `text` text NOT NULL,
  `bgtitle` varchar(15) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`hid`, `cat`, `title`, `text`, `bgtitle`) VALUES
(4, 'basketball', 'RAJEL KBIR , 1928 - 2016', '&lt;p&gt;&lt;strong&gt;Lorem ipsum dolor&lt;/strong&gt; sit amet, consectetur adipiscing elit. Duis magna nisi&lt;br /&gt;Semper sed dapibus in, dictum et purus. Nam et bibendum dui interdum &lt;br /&gt;erat felis, et consectetur turpis aliquam ut. Mauris eu accumsan &lt;br /&gt;&lt;strong&gt;Maecenas aliquam&lt;/strong&gt; nisl nec tincidunt eleifend. Proin quis&lt;br /&gt;eleifend libero, &lt;br /&gt;eu posuere eros. Ut pharetra, ex sit amet &lt;strong&gt;convallis iaculis&lt;/strong&gt; lacus&amp;nbsp; nisi .&lt;br /&gt;&lt;br /&gt;&lt;/p&gt;', 'MR KBIR'),
(5, 'basketball', 'FOULEN FOULENI , 1919 - 1999', '&lt;p&gt;There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don''t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn''t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.&lt;/p&gt;', 'MR PERSON');

-- --------------------------------------------------------

--
-- Table structure for table `player`
--

CREATE TABLE IF NOT EXISTS `player` (
`pid` int(11) NOT NULL,
  `cat` varchar(50) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `height` int(11) NOT NULL,
  `weight` float NOT NULL,
  `post` varchar(50) NOT NULL,
  `jersey` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `player`
--

INSERT INTO `player` (`pid`, `cat`, `nom`, `prenom`, `date`, `height`, `weight`, `post`, `jersey`) VALUES
(8, 'volleyball', 'sissoko', 'mamado', '1982-04-18', 192, 88, 'Pivot', 17),
(9, 'football', 'Trabelssi', 'Youssef', '2000-12-12', 180, 80, 'Gardien', 16),
(10, 'basketball', 'Louzir ', 'Omar ', '1933-08-10', 182, 70, 'ArriÃ¨re gauche', 12);

-- --------------------------------------------------------

--
-- Table structure for table `sondage`
--

CREATE TABLE IF NOT EXISTS `sondage` (
`sid` int(11) NOT NULL,
  `question` text NOT NULL,
  `fanswer` varchar(100) NOT NULL,
  `sanswer` varchar(100) NOT NULL,
  `nbfanswer` int(11) DEFAULT '0',
  `nbsanswer` int(11) DEFAULT '0',
  `pfanswer` float DEFAULT '0',
  `psanswer` float DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sondage`
--

INSERT INTO `sondage` (`sid`, `question`, `fanswer`, `sanswer`, `nbfanswer`, `nbsanswer`, `pfanswer`, `psanswer`) VALUES
(8, 'Quel est le meilleur joueur Ã  l''ASM?', 'Vladimir ', 'Mohsni', 0, 0, 0, 0),
(9, 'Qui est le meilleur joueur a l''ASM?', 'Mouelhi', 'Chaouachi', 26, 18, 60, 40);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
`stid` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `post` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`stid`, `nom`, `prenom`, `post`) VALUES
(1, 'Foulen', 'Fouleni', 'Direction générale'),
(2, 'Drapper', 'Don', 'Responsable'),
(3, 'White', 'Walter', 'Dirigeant'),
(4, 'Lorem', 'Ipsum', 'Coordinateur'),
(5, 'MrPresidente', 'Pablo', 'President'),
(6, 'MORGAN', 'Dexter', 'Executif');

-- --------------------------------------------------------

--
-- Table structure for table `trophy`
--

CREATE TABLE IF NOT EXISTS `trophy` (
`trid` int(11) NOT NULL,
  `cat` varchar(50) NOT NULL,
  `trname` varchar(100) NOT NULL,
  `season` year(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trophy`
--

INSERT INTO `trophy` (`trid`, `cat`, `trname`, `season`) VALUES
(20, 'football', 'Coupe', 2001),
(22, 'football', 'Championnat', 2005),
(24, 'basketball', 'Championnat', 1995),
(25, 'basketball', 'Coupe d''afrique', 1998),
(26, 'football', 'Coupe d''afrique', 1996),
(27, 'football', 'Ligue', 1996);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`uid` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` text NOT NULL,
  `salt` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
`vid` int(11) NOT NULL,
  `link` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`vid`, `link`) VALUES
(1, 'oN2Xs-MvxLw'),
(2, 'gu4H-ZX8lPE'),
(3, '9gSoEHWDvM8');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actualite`
--
ALTER TABLE `actualite`
 ADD PRIMARY KEY (`acid`), ADD KEY `cat` (`cat`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`aid`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`cat`);

--
-- Indexes for table `clubs`
--
ALTER TABLE `clubs`
 ADD PRIMARY KEY (`clid`), ADD KEY `cat` (`cat`), ADD KEY `cat_2` (`cat`);

--
-- Indexes for table `fixtures`
--
ALTER TABLE `fixtures`
 ADD PRIMARY KEY (`fid`), ADD KEY `opponent` (`clid`), ADD KEY `clid` (`clid`);

--
-- Indexes for table `flashinfo`
--
ALTER TABLE `flashinfo`
 ADD PRIMARY KEY (`flid`), ADD KEY `cat` (`cat`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
 ADD PRIMARY KEY (`hid`), ADD KEY `cat` (`cat`);

--
-- Indexes for table `player`
--
ALTER TABLE `player`
 ADD PRIMARY KEY (`pid`), ADD KEY `cat` (`cat`);

--
-- Indexes for table `sondage`
--
ALTER TABLE `sondage`
 ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
 ADD PRIMARY KEY (`stid`);

--
-- Indexes for table `trophy`
--
ALTER TABLE `trophy`
 ADD PRIMARY KEY (`trid`), ADD KEY `cat` (`cat`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
 ADD PRIMARY KEY (`vid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actualite`
--
ALTER TABLE `actualite`
MODIFY `acid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `aid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `clubs`
--
ALTER TABLE `clubs`
MODIFY `clid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `fixtures`
--
ALTER TABLE `fixtures`
MODIFY `fid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `flashinfo`
--
ALTER TABLE `flashinfo`
MODIFY `flid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
MODIFY `hid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `player`
--
ALTER TABLE `player`
MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `sondage`
--
ALTER TABLE `sondage`
MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
MODIFY `stid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `trophy`
--
ALTER TABLE `trophy`
MODIFY `trid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
MODIFY `vid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `actualite`
--
ALTER TABLE `actualite`
ADD CONSTRAINT `actualite_ibfk_1` FOREIGN KEY (`cat`) REFERENCES `category` (`cat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `clubs`
--
ALTER TABLE `clubs`
ADD CONSTRAINT `clubs_ibfk_1` FOREIGN KEY (`cat`) REFERENCES `category` (`cat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fixtures`
--
ALTER TABLE `fixtures`
ADD CONSTRAINT `fixtures_ibfk_1` FOREIGN KEY (`clid`) REFERENCES `clubs` (`clid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `flashinfo`
--
ALTER TABLE `flashinfo`
ADD CONSTRAINT `flashinfo_ibfk_1` FOREIGN KEY (`cat`) REFERENCES `category` (`cat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `history`
--
ALTER TABLE `history`
ADD CONSTRAINT `history_ibfk_1` FOREIGN KEY (`cat`) REFERENCES `category` (`cat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `player`
--
ALTER TABLE `player`
ADD CONSTRAINT `player_ibfk_1` FOREIGN KEY (`cat`) REFERENCES `category` (`cat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trophy`
--
ALTER TABLE `trophy`
ADD CONSTRAINT `trophy_ibfk_1` FOREIGN KEY (`cat`) REFERENCES `category` (`cat`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
