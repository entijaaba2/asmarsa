<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Ajouter une section - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<?php
include_once("connect_to_base.php");
if((isset($_POST))&&(!empty($_POST))){
	$bdd->query('INSERT into category(cat) VALUES ("'.$_POST['cat'].'")');
	header('Location: addCategory.php?r=success');
	die();
} 

?>

<style>
td{
	padding-top: 15px;
}
</style>
</head> 
   
 <body class="sticky-header left-side-collapsed"  onload="initMap()">
    <section>
    <!-- left side start-->
	<?php 
	$page="category";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="switches">
		<div class=" col-lg-6 col-lg-offset-3" >
		<h1 style="text-align: center; margin: 0 auto;">Ajouter une section</h1><br>
		<form class="navbar-form navbar-left" role="search" action="addCategory.php" method="post" >
		
      
	  <div style="padding-left: 150px;">
		  <table>
			<tr>
				<td align="right"> Nom de la section : &nbsp&nbsp</td>
				<td>
				  <div class="form-group">
					<input type="text" name="cat" required class="form-control" placeholder="Nom de la section">
				  </div>
				</td>
			</tr>
			
			
		</table>
		 <br>
		<div id="felsa">
		 <button  class="btn btn-success" style="margin-left: 150px;" type="submit">Ajouter</button>
		</div>
		</div>
		 </form>
		  

		
		
		</div>
		</div>
		
		
		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
   
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
<!-- Modal content-->
   <div <?php if (!empty($_GET)) {if ($_GET['r']=="success") echo 'class="alert alert-success"'; else echo 'class="alert alert-danger"';}?>>
    <h1 style="text-align :center ;"><?php if (!empty($_GET)) {if ($_GET['r']=="success") echo 'Opération réussie'; else { if ($_GET['r']=="failure") echo 'Echec de l\'opération'; else echo '404'; } } ?></h1> 
<br>
<br>
		<h5 style="text-align :center ;"><?php if (!empty($_GET)) { if ($_GET['r']=="success") echo 'Section ajoutée avec succès!'; else echo 'Une erreur est survenue';}?></h5>
  </div>
   </div>
</div>
 <!-- JS Files -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
function modalShow(){
	if(window.location.search){
	$('#myModal').modal('show');
	}
}
modalShow();

</script>


<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>

</body>
</html>