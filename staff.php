  <!DOCTYPE html>
  <html lang="en">
  <head>
   <title>Staff - Avenir Sportif de la Marsa</title>
   <meta charset="utf-8-bom">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
   <link rel="icon" type="image/png" href="favicons/favicon-32x32.png" sizes="32x32">
   <link rel="icon" type="image/png" href="favicons/favicon-16x16.png" sizes="16x16">
   <link rel="manifest" href="favicons/manifest.json">
   <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#5bbad5">
   <meta name="theme-color" content="#ffffff">
   <link rel="stylesheet"  href="css/bootstrap.css">
   <link rel="stylesheet" href="css/styles.css">
   <link rel="stylesheet" href="css/ionicons.min.css">
   <link rel="stylesheet" href="css/jquery-ui.css">
   <link rel="stylesheet" href="css/font-awesome.min.css">
   <meta name="robots" content="index,follow,noodp"><!-- All Search Engines -->
   <meta name="googlebot" content="index,follow"><!-- Google Specific -->
   <link rel="stylesheet" href="css/styles-squad.css">
   <link rel="stylesheet" href="css/staff.css">
 </head>
 <body>
  <!-- ******************** NAV *********************** -->
 <?php 
 include_once('connect_to_base.php');
 include('nav_lin.php');?>

<!-- ******************** SQUAD *********************** -->

<div class="container main-slider" id="asm">
  <br>
<br>
<br>
<div class="headertitle">STAFF</div>
<div class="row col-lg-10 col-lg-offset-1">
<?php
$president=$bdd->query('SELECT * FROM staff WHERE post="President"')->fetch();
if ($president){
?>
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-12">
<div class="flipeer-container">
<div class="flip-container flipeer">
  <div>
    <div class="front" style="background: url(./img/staff/<?php echo $president['stid'];?>.jpg) 0 0 no-repeat;">
    </div>
  </div>
  <div class="downinfo">
  <div class="player_name"><strong class="player"> <?php echo $president['nom']." ".$president['prenom'];?></strong><span class="post"><?php echo $president['post'];?></span></div>
  </div>
</div>
</div>

</div>
<?php 
} 
  $req=$bdd->query('SELECT * FROM staff WHERE post<>"President"');
  while($data=$req->fetch()){
  ?>
<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
<div class="flipeer-container">
<div class="flip-container flipeer">
  <div>
    <div class="front" style="background: url(./img/staff/<?php echo $data['stid'];?>.jpg) 0 0 no-repeat;">
    </div>
  </div>
  <div class="downinfo">
  <div class="player_name"><strong class="player"> <?php echo $data['nom']." ".$data['prenom'];?></strong><span class="post"><?php echo $data['post'];?></span></div>
  </div>
</div>
</div>

</div>

<?php } ?>
</div>
</div>

<!-- ******************** FOOTER *********************** -->
<?php include('footer.php');?>
<!-- ******************** JS *********************** -->


<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/custom-squad.js"></script>


</body>
</html>