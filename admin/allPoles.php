<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Liste des sondages - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
</head> 
   
 <body class="sticky-header left-side-collapsed"  onload="initMap()">
    <section>
    <!-- left side start-->
	<?php 
	$page="pole";
	include("navbar.php");
	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
				<div class="graphs">
			<!-- switches -->
		<div class="col-lg-10">
		<h2>Liste des sondages effectués:</h2>
		</div>
		
		<div class="col-lg-2">
		<a href="sondage.php"><button class="btn btn-success">Ajouter un sondage</button></a>
		</div>
		
		<div class="switches">
		<div class=" col-lg-12">
		
		<br>
      <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Question</th>
                <th>Première réponse</th>
                <th>Nb votes 1ère réponse</th>
                <th>(%) 1ère réponse</th> 
				<th>Deuxième réponse</th>
                <th>Nb votes 2ème réponse</th>
                <th>(%) 2ème réponse</th>
                <th>Nb total de votes</th>
                <th>Supprimer</th>
            </tr>
        </thead>
        
        <tbody>
		<?php
		include('connect_to_base.php');
		$req=$bdd->query('SELECT * FROM sondage ORDER BY sid DESC');
		while($data=$req->fetch()){
		?>
         <tr>
                <td><?php echo $data['question']; ?></td>
                <td><?php echo $data['fanswer']; ?></td>
                <td><?php echo intval($data['nbfanswer']); ?> vote(s)</td>
                <td><?php echo floatval($data['pfanswer']); ?> %</td>
                <td><?php echo $data['sanswer']; ?></td>
                <td><?php echo intval($data['nbsanswer']); ?> vote(s)</td>
                <td><?php echo floatval($data['psanswer']); ?> %</td>
                <td><?php echo intval($data['nbfanswer'])+intval($data['nbsanswer']); ?> vote(s)</td>
                <td  style="text-align : center;"><a href="deletePole.php?sid=<?php echo $data['sid'];?>"><button class="btn btn-danger btn-xs" data-toggle="tooltip" title="supprimer le sondage"><i class="fa fa-trash-o " ></i></button></a></td>
                
            </tr>
		<?php } ?>
		</tbody>
		</table>
		
		</div>
		</div>
		

		<!-- //switches -->
				</div>
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
   

 <!-- JS Files -->
<script src="js/jquery.js"></script>
 <script src="js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
	$('#example').DataTable();
});

</script>
<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>

</body>
</html>