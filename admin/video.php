<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Vidéo index - Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 <!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
</head> 
   
 <body class="sticky-header left-side-collapsed">
    <section>
    <!-- left side start-->
	<?php 
	$page="video";
	include("navbar.php");
	$msg="";
	if (isset($_GET['m'])){
		switch($_GET['m']){
			case 1 :
				$msg="Une erreur est survenue!";
				break;
			case 2 :
				$msg="Vidéo ajoutée avec succès!";
				break;
		}
	
	}

	if (isset($_POST)&&(!empty($_POST))){
	include_once("connect_to_base.php");
	$tmp=explode('v=',$_POST['lien']);
	$bdd->query('INSERT INTO video(link) VALUES("'.$tmp[1].'")');
	header("Location: video.php?m=2");
	die();
	}
	


	?>
		<!-- left side end-->
    
		<!-- main content start-->
		<div class="main-content">
			<!-- header-starts -->
			<?php include("header.php"); ?>
			<!-- //header-ends -->
			<div id="page-wrapper">
			
		<div class="col-lg-12" style="text-align : center;">
		<h4>Entrez le lien YouTube de la vidéo que vous désirez partager sur la page d'accueil :</h4>
      <form class="navbar-form navbar-left" style="padding-left : 450px;" role="search" action="video.php" method="post">
		  <div class="form-group">
			<input style="width : 450px; " type="text" name="lien" required class="form-control" placeholder="lien..."> <br><br>
		  <button type="submit" class="btn btn-info">Valider</button>
		  </div>
		  
	  </form>

	</div>
	
	
			<!--body wrapper start-->
			</div>
			 <!--body wrapper end-->
		</div>
        <!--footer section start-->
			<footer>
			   <p>&copy 2016 G-dice </p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
   <div <?php if ($_GET['m']==2) echo 'class="alert alert-success"'; else echo 'class="alert alert-danger"';?>>
    <h1 style="text-align :center ;"><?php if ($_GET['m']==2) echo 'Opération réussie'; else { if (($_GET['m']==1) ||($_GET['m']==3) ||($_GET['m']==4)) echo 'Echec de l\'opération'; else echo '<h1>404</h1>'; } ?></h1> 
<br>
<br>
		<h5 style="text-align :center ;"><div  id="msgModal"></div></h5>
  </div>
  </div>
</div>  

 <!-- JS Files -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
function modalShow(){
	if(window.location.search){
	var msg="<?php echo $msg;?>";
	document.getElementById('msgModal').innerHTML=msg;
	$('#myModal').modal('show');
	}
}
modalShow();			
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
<!-- END JS Files --> 
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
   
</body>
</html>